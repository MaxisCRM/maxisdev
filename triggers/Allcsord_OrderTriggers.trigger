trigger Allcsord_OrderTriggers on csord__Order__c (after delete, after insert, after update, before delete, before insert, before update) {
    TriggerFactoryInt.createHandler(AllOrderTriggerHandlerInt.class, csord__Order__c.getSObjectType());
}