trigger SmsLogTrigger on SMS_Log__c (after update) 
{
    System.debug('Testing to see if trigger');
    for (SMS_Log__c log: Trigger.new)
    {
        System.debug(log);
        if (log.status__c == 'Sent')
        {
            System.debug('trigger before send');
            SmsCalloutClass.SendSmsToExternalSystem(Trigger.new);
            System.debug('trigger sent');
        }
       else
        {
            System.debug('trigger not sent');
        }
    }
}