trigger ProductBasketTrigger on cscfga__Product_Basket__c (after delete, after insert, after update, 
before delete, before insert, before update) 
{
    No_Triggers__c notriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
    
    if (notriggers == null || !notriggers.Flag__c)
    {

        if ((trigger.isInsert) && (trigger.IsAfter))
        {
            ProductBasketTriggerHandler.AfterInsertHandle(trigger.new);
            system.debug('****after insert trigger.new=' + trigger.new);
        }
        
        if ((trigger.isUpdate) && (trigger.IsAfter))
        {
            ProductBasketTriggerHandler.AfterUpdateHandle(trigger.new, trigger.old);
            system.debug('****after udate trigger.old=' + trigger.old);
            system.debug('****after udate trigger.new=' + trigger.new);
            
        }
        if ((trigger.isInsert) && (trigger.isBefore))
        {
            ProductBasketTriggerHandler.beforeInsertHandle(trigger.new);
            //system.debug('****after udate trigger.old=' + trigger.old);
            system.debug('****after udate trigger.new=' + trigger.new);
        }
        
    }
}