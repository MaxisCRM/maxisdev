trigger AllProductBasketCustomTrigger on cscfga__Product_Basket__c(after delete, after insert, after update, before delete, before insert, before update) {
    TriggerFactoryInt.createHandler(AllProductBasketCustomTriggerHandlerInt.class, cscfga__Product_Basket__c.getSObjectType());
}