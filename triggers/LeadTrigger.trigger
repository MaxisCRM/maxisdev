trigger LeadTrigger on Lead (before insert) {
    LeadBulkImportTriggerController lbitc = new LeadBulkImportTriggerController();
    if(Trigger.isInsert && Trigger.isBefore){
        lbitc.checkOwner(Trigger.new);
    }

}