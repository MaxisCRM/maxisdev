({
	doInit : function(component, event, helper) {
		component.set('v.columns', [  
            {label: 'Solution', fieldName: 'Solution', type: 'text'},
            {label: 'Name', fieldName: 'Name', type: 'text'},
            {label: 'MSISDN/Telephone Number', fieldName: 'tNumber', type: 'text'},
            {label: 'Pilot', fieldName: 'Pilot',type: 'boolean'},
            {label: 'Hunting', fieldName: 'Hunting', type: 'boolean' }
            
        ]);
        component.set('v.searchColumns', [  
            {label: 'Available Number', fieldName: 'availNumber', type: 'text'}
            
        ]);
        helper.populateValues(component);
		helper.populateNumberAssgnValues(component);
	},
    handleClick : function (component, event, helper) {
        component.set("v.showError",false);
        var buttonName = event.getSource().get("v.label");
        if(buttonName == 'Clear'){
            helper.clearSearch(component);
        }
        else if(buttonName == 'Search'){
            helper.doSearch(component);
        }
        else if(buttonName == 'Assign Numbers'){
            helper.assignNumbers(component);
        }
        else if(buttonName == 'Remove Assigned'){
            helper.removeAssigned(component);
        }
        else if(buttonName == 'Reserve Selected'){
            helper.reserveSelected();
        }
    },
    handleRowSelection : function(component, event, helper){
        var selectedRows = event.getParam('selectedRows');
        var voiceIds = [];
        for(var row in selectedRows){
            voiceIds.push(selectedRows[row].id);
        }
        component.set("v.voiceIds",voiceIds);
        
    },
    handleNumberSelection : function(component, event, helper){
       var selectedRows = event.getParam('selectedRows');
        component.set("v.selectedNumbers",selectedRows); 
    }
    
})