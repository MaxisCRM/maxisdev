({
	clearSearch : function(component) {
		component.set("v.selectedQuantity",null);
        component.set("v.selectedCity",null);
        component.set("v.selectedPostCode",null);
        component.set("v.availableNumbersList",null);
	},
    doSearch : function(component) {
		var postCode = component.get("v.selectedPostCode");
        var city = component.get("v.selectedCity");
        var quantity = component.get("v.selectedQuantity");
        var action = component.get("c.searchAvailableNumbers");
        action.setParams({ "postCode" : postCode,
                           "city" : city,
                           "quantity" : quantity,
                         });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.availableNumbersList",response.getReturnValue());
            } 
            else {
                var errors = response.getError();                       
                component.set("v.showError",true);
                component.set("v.errorMessage",errors[0].message);
            }
        });
        $A.enqueueAction(action);
	},
    assignNumbers : function(component) {
        var voiceIds = component.get("v.voiceIds");
        var availableNumbers = JSON.stringify(component.get("v.selectedNumbers"));
		var action = component.get("c.assignNumbers");
        console.log(voiceIds);
        console.log(availableNumbers);
        action.setParams({ "voiceIds" : voiceIds,
                           "jsonList" : availableNumbers
                         });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue()){
                    var responseMap = response.getReturnValue();
                    var pcWrapList = component.get("v.pcWrapList");
                    for(var i=0;i<pcWrapList.length;i++){
                        if(voiceIds.includes(pcWrapList[i].id))
                        	pcWrapList[i].tNumber = responseMap[pcWrapList[i].id];
                    }
                    component.set("v.pcWrapList",pcWrapList);
                }
            } 
            else {
                var errors = response.getError();                       
                component.set("v.showError",true);
                component.set("v.errorMessage",errors[0].message);
            }
        });
        $A.enqueueAction(action);
	},
    removeAssigned : function(component) {
		var voiceIds = component.get("v.voiceIds");
        var pcWrapList = component.get("v.pcWrapList");
        for(var i=0;i<pcWrapList.length;i++){
            if(voiceIds.includes(pcWrapList[i].id))
            	pcWrapList[i].tNumber = null;
        }
        component.set("v.pcWrapList",pcWrapList);
	},
    reserveSelected : function() {
		
	},
    populateValues : function(component){
        var basketId = component.get("v.basketId");
        var defaultSite = component.get("v.defaultSite");
        
        var action = component.get("c.prePopulateValues");
        action.setParams({ "basketId" : basketId,"defaultSite":defaultSite });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue()){
                    var responseList = response.getReturnValue();
                    var pcList = [];
                    var cityList = [];
                    for(var i in responseList){
                        pcList.push(responseList[i].postCode);
                        cityList.push(responseList[i].cityName);
                    }
                	component.set("v.postCodeOptions",pcList);
                    component.set("v.cityOptions",cityList);
                    
                }
            } 
            else {
                console.log(state);
            }
        });
        $A.enqueueAction(action);
    },
    populateNumberAssgnValues : function(component){
        var voiceList = [];
        voiceList = component.get("v.voiceList");
        var displayList = [];
        var solutionName = component.get("v.solutionName");
        for(var x in voiceList){ 
            displayList.push({Solution: solutionName , Name: voiceList[x].name, id:voiceList[x].id, tNumber: null,Pilot: (voiceList[x].pilot=='true'?true:false), Hunting: (voiceList[x].pilot=='true'?false:true) });
        }
        component.set("v.pcWrapList",displayList);
    }
})