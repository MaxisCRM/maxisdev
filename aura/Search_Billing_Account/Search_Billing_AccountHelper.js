({
    showToast : function(component, message, title,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },
    navigateToAccount: function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get('v.accountId'),
            "slideDevName": "Related"
        });
        navEvt.fire();        
    },
    retrieveBillingAccountHelper : function(cmp, event, helper) { 
        console.log('inside api caller helper ::>')
    	var xmlHttp = new XMLHttpRequest();
    	var url = 'https://th-apex-http-callout.herokuapp.com/animals'
    	xmlHttp.open( "GET", url, true );
		xmlHttp.onload = function () {
    	    console.log("onload");
        	console.log(xmlHttp.readyState);
        	console.log(xmlHttp.status);
    		if (xmlHttp.readyState === 4) {
    	    	if (xmlHttp.status === 200) {
	            	console.log(xmlHttp.response);
            		console.log(xmlHttp.responseText);
		        }
	    	}
		};
	    xmlHttp.send( null );
	    console.log("Request sent");
    }
})