({
	onPageReferenceChange : function(component, event, helper) {
		var myPageRef = component.get("v.pageReference");
        var accId = myPageRef.state.c__accountId;
        component.set('v.accountId',accId);
        helper.retrieveBillingAccountHelper(component, event, helper);
	},
    navigationHandler : function(component, event, helper) {
        helper.navigateToAccount(component, event, helper);
    },
    addBillingAccount : function(component, event, helper) {
        helper.showToast(component, 'Billing account added succesfully', 'Success!', 'success');
        helper.navigateToAccount(component, event, helper);
    },
    createBillingAccount: function(component, event, helper) {        
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "csconta__Billing_Account__c"
        });
        createRecordEvent.fire();
    },
    selectBillingAccount: function(component, event, helper) {
       var selected = event.getSource().get("v.value");     
        console.log('value ::>'+selected);
    },
    thirdPartCallHandler: function(component, event, helper){
        helper.retrieveBillingAccountHelper(component, event, helper);
    }
})