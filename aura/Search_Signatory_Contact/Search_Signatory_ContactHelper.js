({
    showToast : function(component, message, title,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },
    navigateToAccount: function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get('v.accountId'),
            "slideDevName": "related"
        });
        navEvt.fire();        
    },
    gotoRelatedList : function (component, event, helper) {
    var relatedListEvent = $A.get("e.force:navigateToRelatedList");
    relatedListEvent.setParams({
        "relatedListId": "AccountContactRelations",
        "parentRecordId": component.get("v.accountId")
    });
    relatedListEvent.fire();
},
    invokeserversidecontroller : function (component, event, helper){
      
        var myPageRef = component.get("v.pageReference");
        var accId = myPageRef.state.c__accountId;
		component.set('v.accountId',accId);
     var action = component.get("c.searchcontacts");
        action.setParams({ accid : component.get("v.accountId") });
         action.setCallback(this, function(response) {
           
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
				component.set('v.contactlist',response.getReturnValue());
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
	addsignatory : function (component, event, helper) {
         var action = component.get("c.addSFsignatory");
        action.setParams({ accid : component.get("v.accountId") , roleid : component.get("v.selectedsignatories"), contacts : component.get("v.contactlist") });
         action.setCallback(this, function(response) {
           
            var state = response.getState();
            if (state === "SUCCESS") {
                
     helper.showToast(component, 'Signatory contact updated succesfully', 'Success!', 'success');
        helper.navigateToAccount(component, event, helper);
            }
         });
         $A.enqueueAction(action);
    },
    createsignatory : function (component, event, helper) {
        //alert('yes');
         var action = component.get("c.createSFcontact");
        action.setParams({ accid : component.get("v.accountId") });
         action.setCallback(this, function(response) {
           
            var state = response.getState();
            if (state === "SUCCESS") {
               alert( response.getReturnValue());
     //helper.showToast(component, 'Signatory contact added succesfully', 'Success!', 'success');
        helper.navigateToAccount(component, event, helper);
            }
         });
         $A.enqueueAction(action);
    }              
})