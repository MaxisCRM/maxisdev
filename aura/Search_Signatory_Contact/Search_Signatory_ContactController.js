(
    {
	onPageReferenceChange : function(component, event, helper) {
				 //alert('pre server side');

        helper.invokeserversidecontroller(component, event, helper);
		 //alert('post server side');
	},
    navigationHandler : function(component, event, helper) {
        helper.navigateToAccount(component, event, helper);
    },
    addsignatorycontact : function(component, event, helper) {
        helper.addsignatory(component, event, helper);
       
    },
    createsignatorycontact: function(component, event, helper) {  
        var myPageRef = component.get("v.pageReference");
        var accId = myPageRef.state.c__accountId;
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Contact",
             "defaultFieldValues": {
        'Authorized_Signatory__c' : true,
                 'AccountId' : accId
             }, "navigationLocation": "LOOKUP",
            "panelOnDestroyCallback": function(event){
                 helper.createsignatory(component, event, helper);

            }
        });
        createRecordEvent.fire();
       // helper.createsignatory(component, event, helper);
        
        
    },
    selectSignatoryContact: function(component, event, helper) {
       var selected = event.getSource().get("v.name");  
       // var checkCmp = component.find("selected");
		component.set("v.selectedsignatories",selected);
       // console.log('value ::>'+checkCmp);
       // component.set("v.selectedsignatories","");
           // component.set("v.selectedsignatories",event.getSource().get("v.value"));
        console.log('value ::>'+component.get("v.selectedsignatories"));

    },
})