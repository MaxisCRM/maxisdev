global class CheckCreator extends csbb.CustomButtonExt {
   
   /* global String performAction (String basketId) {
        csdg.DocGenerationWrapper result;
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, String> constantsMap = new Map<String, String>();
        
        inputMap.put('recordId', basketId);
        csdg__Data_Source__c constantsDS = [SELECT Id FROM csdg__Data_Source__c WHERE Name = 'Fixed Check Source' Limit 1];
        constantsMap.put(constantsDS.Id, constantsDS.Id);
        inputMap.put(constantsDS.Id, constantsMap);
        //inputMap.put('userId', System.UserInfo.getUserId());
        result = csdg.DocumentDataHandler.generate('a3M0T0000004rFa',basketId , inputMap);
        String newUrl = '/servlet/servlet.FileDownload?file=' + result.attachmentId;
        return '{"status":"ok","redirectURL":"' + newUrl + '","text":"Loading Service Order Page"}';
    }*/
    
    global String performAction (String basketId) {
        //csdg.DocGenerationWrapper result;
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, String> constantsMap = new Map<String, String>();
        
        inputMap.put('recordId', basketId);
        csdg__Data_Source__c constantsDS = [SELECT Id FROM csdg__Data_Source__c WHERE Name = 'Fixed Check Source' Limit 1];
        inputMap.put(constantsDS.Id, constantsMap);
        //inputMap.put('userId', System.UserInfo.getUserId());
        //result = csdg.DocumentDataHandler.generate('a3M0T0000004rIA',basketId , inputMap);
        String result = csdg.ConvertController.convertToPdf('a3M0T0000004rFa', basketId);
        system.debug('result =====' + result );
        String newUrl = '/servlet/servlet.FileDownload?file=' + result;
        return '{"status":"ok","redirectURL":"' + newUrl + '","text":"Loading Service Order Page"}';
    }       
    
    webService static String convertToPdf(Id docConfigId, Id objectId) {
        system.debug('docConfig: ' + docConfigId);
        System.debug('objectId: ' + objectId);
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> constantsMap = new Map<String, Object>();
        inputMap.put('recordId', objectId); /*Again create a Map<String, String>. This can contains anything you like. But, formatted to a String */
        
        csdg__Data_Source__c constantsDS = [SELECT Id FROM csdg__Data_Source__c WHERE Name = 'Fixed Check Source' Limit 1];
        constantsMap.put(constantsDS.Id, constantsDS);
        inputMap.put(constantsDS.Id, constantsMap);
        csdg.DocGenerationWrapper result;
        if(docConfigId != null) {
            result = csdg.DocumentDataHandler.generate(docConfigId, objectId, inputMap);
        }
        else {
            result = new csdg.DocGenerationWrapper();
        }
        return result.attachmentId;//return null;
    }
}