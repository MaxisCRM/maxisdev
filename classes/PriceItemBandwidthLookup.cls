global with sharing class PriceItemBandwidthLookup extends cscfga.ALookupSearch {
    public override String getRequiredAttributes() {
        return '["PDName", "ServiceTypeStr","COPAX value"]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {

        //system.debug(LoggingLevel.ERROR, 'CommercialProductLookup: ' + JSON.serialize(searchFields));

        String pdName = searchFields.get('PDName');
       // system.debug(LoggingLevel.ERROR, 'CommercialProductLookup PDName ' + PDName);
     String copaxvalue = searchFields.get('COPAX value') != null ? '%'+searchFields.get('COPAX value')+'%' : null ;
     
        List<cspmb__Price_Item__c> pItems = [
            select Id, Name, Service_Type__c, Bandwidth__c, cspmb__Contract_Term__c
            from cspmb__Price_Item__c 
            where cspmb__Product_Definition_Name__c = :pdName and Service_Type__c = null  and name like :copaxvalue and Bandwidth__c != null order by Bandwidth__c asc
        ];

        // get distinct list
        Map<String,cspmb__Price_Item__c> pItemsMap = new  Map<String,cspmb__Price_Item__c>();
        if(pItems!=null)
        {
            for(cspmb__Price_Item__c item : pItems){
                if(!pItemsMap.containskey(item.Bandwidth__c)){
                pItemsMap.put(item.Bandwidth__c,item);
                }
            }
        } 
        if(pItemsMap!=null && pItemsMap.size()>0)       
          return pItemsMap.Values();
        else
          return null;  
    }
}