/******************************************************
1       VenkataRamanan G      18/12/2019                Generic class to fetch pre-required data for Solution Console
*******************************************************/

global with sharing class SolutionActionHelper implements cssmgnt.RemoteActionDataProvider  {

    @RemoteAction
    global static Map<String,Object> getData(Map<String,Object> inputMap) {
       
        System.debug('SolutionActionHelper Input Map: ' + inputMap);
        //return getRequiredInfo(inputMap);
        
        Map<String, Object> returnMap = new Map<String, Object>();

        Set<String> inputKey = inputMap.keySet();
        for (String k :inputKey) {
        if(k=='GetBasket'){
            String basketId = (String)inputMap.get('GetBasket');    
                System.debug('GetBasket for basketId:  ' + basketId);
                String res = GetBasket(basketId);   
                returnMap.put('GetBasket', res);
        }
        else if (k=='GetSiteId'){
        //System.debug('GetBasket for basketId:  ' + basketId);
                String res = GetSiteId();   
                returnMap.put('GetSiteId', res);
        }
        }
         return returnMap;
        }
        
 global static string GetBasket(String basketId) {

        if (basketId == null || basketId.length() == 0)
            return ''; 
        try {
            cscfga__Product_Basket__c basket = [select Billing_Account__c,csordtelcoa__Change_Type__c, csconta__Contract__r.CustomerSignedDate,csordtelcoa__Basket_Stage__c, csbb__account__c, cscfga__Opportunity__c from cscfga__Product_Basket__c where id =: basketId LIMIT 1];
            if (basket != null) {
                return JSON.serialize(basket); 
            }  
        } catch (Exception e) {
            System.debug('GetBasketChangeType: '+ e.getMessage());
        }
        return '';
    }
     global static string GetSiteId() {
        return Site.getSiteId();
    }
    }