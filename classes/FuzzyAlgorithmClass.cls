public with sharing class FuzzyAlgorithmClass {
   
   public static Double executeFuzzyLogic(List<Company_Abbrevation__mdt> listAbb,String str1, String str2){
   //public static double executeFuzzyLogic(String str1, String str2){
      // lowest score on empty strings
      if (str1 == null || str2 == null || String.isEmpty(str1) || String.isEmpty(str2)) {
         return 0;
      }

      //convert to Uppercase
      str1 = str1.toUpperCase();
      str2 = str2.toUpperCase();

      // highest score on equal strings
      if (str1.equals(str2)) {
         return 100;
      }

      Double dblPercentage = 0.0;
      
      if(listAbb.size() >0){
         //Remove abbrevation in company name (e.g. sdn bhd)
         str1 = removeAbbreviation(listAbb, str1);
         str2 = removeAbbreviation(listAbb, str2);
   
         //compare the similarity of both strings in percentage
         dblPercentage = jaroWinklerDistance(str1, str2);
         if(dblPercentage < 50.0){
            dblPercentage = 0.0;
         }
      }
      return dblPercentage;
   }

   public static String removeAbbreviation(List<Company_Abbrevation__mdt> listAbb, String str){
      str = str.replace('.','');
      for(Company_Abbrevation__mdt abb: listAbb){
         str = str.replace(abb.MasterLabel, '');
      }
      return str;
   }

   //reason for not using the ootb sf function:
   //Recursion that will cause error on CPU time limit exception  
   public static Double levenshteinDistance(String str1, String str2) {
      Integer str1Length = str1.length() + 1;
      Integer str2Length = str2.length() + 1;
      List<Integer> listChar1 = new List<Integer>();
      List<Integer> listChar2 = new List<Integer>();
      for(Integer i = 0; i < str1Length; i++){
         listChar1.add(i);
      }
      for(Integer j = 1; j< str2Length; j++) {
         listChar2.add(j);
         for(Integer i = 1; i< str1Length; i++) {
            Integer match = (str1.charAt(i-1) == str2.charat(j-1)) ? 0 : 1;
            Integer replaced = listChar1[i-1] + match;
            Integer inserted = listChar1[i] + 1;
            Integer deleted = listChar1[i - 1] + 1;
            if(listChar2.size() > i ){
               listChar2[i] = Math.min(Math.min(inserted,deleted),replaced);
            }
            else{
               listChar2.add(Math.min(Math.min(inserted,deleted),replaced));
            }
         }
         List<Integer> swap = new List<Integer>();
         swap.addAll(listChar1);
         listChar1 = new List<Integer>();
         listChar1.addAll(listChar2);
         listChar2 = new List<Integer>();
         listChar2.addall(swap);
      }
      Integer bigger = Math.max(str1.length(),str2.length());
      Double percentage = (double.valueOf(bigger)-double.valueOf(listChar1[str1Length - 1])) * 100 /double.valueOf(bigger);
      return percentage;
   }

   //Result same as levenshteinDistance function
   public static Double weightedLevenshtein(String str1, String str2) {
      Integer str1Length = str1.length() + 1;
      Integer str2Length = str2.length() + 1;
      List<Integer> char1 = new List<Integer>();
      List<Integer> char2 = new List<Integer>();
      for(Integer i = 0; i < str1Length; i++){
         char1.add(i);
      }
      for(Integer j = 1; j< str2Length; j++) {
         char2.add(j);
         for(Integer i = 1; i< str1Length; i++) {
            Integer match = 0;
            
            if(str1.charAt(i-1) == str2.charat(j-1)){
               String a = 'A';
               String z = 'Z';
               match = (Integer)Math.abs((Double)(str1.charAt(i-1) - str2.charAt(j-1))/(z.charAt(0)-a.charAt(0)));
            }else{
               match = 1;
            }
            Integer replaced = char1[i-1] + match;
            Integer inserted = char1[i] + 1;
            Integer deleted = char1[i - 1] + 1;
            if(char2.size() > i ){
               char2[i] = Math.min(Math.min(inserted,deleted),replaced);
            }
            else{
               char2.add(Math.min(Math.min(inserted,deleted),replaced));
            }
         }
         List<Integer> swap = new List<Integer>();
         swap.addAll(char1);
         char1 = new List<Integer>();
         char1.addAll(char2);
         char2 = new List<Integer>();
         char2.addall(swap);
      }
      Integer bigger = Math.max(str1.length(),str2.length());
      Double percentage = (double.valueOf(bigger)-double.valueOf(char1[str1Length - 1])) * 100 /double.valueOf(bigger);
      return percentage;
   }

   public static Double jaroWinklerDistance(final String str1, final String str2) {
      // some score on different strings
      Integer prefixMatch = 0; // exact prefix matches
      Integer matches = 0; // matches (including prefix and ones requiring transpostion)
      Integer transpositions = 0; // matching characters that are not aligned but close together 
      Integer maxLength = Math.max(str1.length(), str2.length());
      Integer maxMatchDistance = Math.max((Integer) Math.floor(maxLength / 2.0) - 1, 0); // look-ahead/-behind to limit transposed matches
      // comparison
      final String shorter = str1.length() < str2.length() ? str1 : str2;
      final String longer = str1.length() >= str2.length() ? str1 : str2;
      for (Integer i = 0; i < shorter.length(); i++) {
            // check for exact matches
         Boolean match = shorter.charAt(i) == longer.charAt(i);
         if (match) {
            if (i < 4) {
               // prefix match (of at most 4 characters, as described by the algorithm)
               prefixMatch++;
            }
            matches++;
            continue;
         }
         // check fro transposed matches
         for (Integer j = Math.max(i - maxMatchDistance, 0); j < Math.min(i + maxMatchDistance, longer.length()); j++) {
            if (i == j) {
               // case already covered
               continue;
            }
            // transposition required to match?
            match = shorter.charAt(i) == longer.charAt(j);
            if (match) {
               transpositions++;
               break;
            }
         }
      }
      // any matching characters?
      if (matches == 0) {
         return 0;
      }
      // modify transpositions (according to the algorithm)
      transpositions = (Integer) (transpositions / 2.0);
      // non prefix-boosted score
      Double score = 0.3334 * (matches / (Double) longer.length() + matches / (Double) shorter.length() + (matches - transpositions)
            / (Double) matches);
      if (score < 0.7) {
         return score;
      }
      // we already have a good match, hence we boost the score proportional to the common prefix
      Double boostedScore = score + prefixMatch * 0.1 * (1.0 - score);
      return boostedScore*100;
   }

   public static Double jaccardDistance(String str1, String str2){
      final Set<Integer> str1Set = new Set<Integer>();
      for (Integer i = 0; i < str1.length(); i++) {
         str1Set.add(str1.charAt(i));
      }
      final Set<Integer> str2Set = new Set<Integer>();
      for (Integer i = 0; i < str2.length(); i++) {
         str2Set.add(str2.charAt(i));
      }
      final Set<Integer> unionSet = new Set<Integer>(str1Set);
      unionSet.addAll(str2Set);
      final Integer intersectionSize = str1Set.size() + str2Set.size() - unionSet.size();
      return (100 * intersectionSize / unionSet.size());
   }

   public static Double hammingDistance(String str1, String str2){
      Integer distance = 0;
      for (Integer i = 0; i < str1.length(); i++) {//str1 and str2 must be of same length
         if (str1.charAt(i) != str2.charAt(i)) {
            distance++;
         }
      }
      Integer longer = Math.max(str1.length(),str2.length());
      Double percent = (Double.valueOf(longer)-Double.valueOf(distance)) * 100 /Double.valueOf(longer);
      return percent;
   }

   public static Double cosineSimilarity(String str1, String str2){
      Double dotProduct = 0.0;
      Double magnitude1 = 0.0;
      Double magnitude2 = 0.0;
      Double result = 0.0;

      for (Integer i = 0; i < str1.length(); i++) //str1 and str2 must be of same length
      {
         dotProduct += str1.charAt(i) * str2.charAt(i);  //a.b
         magnitude1 += Math.pow(str1.charAt(i), 2);  //(a^2)
         magnitude2 += Math.pow(str2.charAt(i), 2); //(b^2)
      }

      magnitude1 = Math.sqrt(magnitude1);//sqrt(a^2)
      magnitude2 = Math.sqrt(magnitude2);//sqrt(b^2)

      if (magnitude1 != 0.0 | magnitude2 != 0.0)
      {
         result = dotProduct / (magnitude1 * magnitude2);
      } 
      return result;
   }
}