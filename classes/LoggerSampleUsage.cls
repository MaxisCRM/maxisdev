/**
 * Name: LoggerSampleUsage
 * Author: Accenture
 * Created Date: 11 Dec 2019
 * Description: Sample code to show how to use the Logger Utility
 * History:[Author] [Modified Date] [Modification Reason]
**/
public class LoggerSampleUsage {
    
    /**
    * Method Name: doNormalLogging
    * Description: Log debug message, information message 
    **/
    public static void doNormalLogging()
    {
        LogDebug log = new LogDebug(LoggerSampleUsage.class.getName(), 'Normal');
		log.debug('===> Start doNormalLogging...');
        
        method1(log);
        
        method2(log);
        
        log.debug('===> End doNormalLogging...');
        log.save();
    }

    /**
    * Method Name: doExceptionLogging
    * Description: Log a exception
    **/
    public static void doExceptionLogging()
    {
        LogDebug log = new LogDebug(LoggerSampleUsage.class.getName(), 'Exception');
        log.debug('===> Start doExceptionLogging...');
        String relatedRecords= '5009000001LKBpF,00B90000009LG0c';
        
        try
        {
        /* Test SObjectException    */
          	Log__c m = [SELECT Method__c FROM Log__c LIMIT 1];
           Double inventory = m.math__c;
            
         /* Test MathException    */   
          // Decimal error = 3 / 0;
        
         /* Test NullPointerException    */ 
       // String s;
		//s.toLowerCase();
        }
        catch(Exception e)
        {
            log.log(e, relatedRecords);
        }
        log.debug('===> End doExceptionLogging...');
        log.save();
    }
   
    private static void method1(LogDebug log)
    {
        String relatedRecords= 'Record ID 1,Record ID 2';
        log.info('In method1...', relatedRecords);
    }
    
    private static void method2(LogDebug log)
    {
        String relatedRecords= 'Record ID 3,Record ID 4';
        log.error('In method2...', relatedRecords);
    }
}