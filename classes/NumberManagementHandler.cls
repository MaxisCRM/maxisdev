/*************************************************************************************************
EDGE -93081
Class Name: NumberManagementComp
Description: Handler for Number Management UI on Product Basket 
Author:Sandip Deshmane
***************************************************************************************************/

public with sharing class NumberManagementHandler {
    
    private static string [] PROD_SPEC_VALUES=new string[]{'DMCAT_ProductSpecification_000419','DMCAT_ProductSpecification_000420'};
    private static string [] NGUC_PROD_SPEC_VALUES=new string[]{'DMCAT_ProductSpecification_000871'};
    private static string NUMBER_STATUS = 'DISCONNECT PENDING';
    private static string STATUS_ACTIVE = 'ACTIVE';
    
        
    //Wrapper to get Number Object 
    public class NumberWrapper{
        @AuraEnabled
        public string numberId{get;set;}
        @AuraEnabled
        public String PhoneNumber{get;set;}
        @AuraEnabled
        public boolean IsSelected{get;set;}
        @AuraEnabled
        public String ProdConfigId{get;set;}
        @AuraEnabled
        public Boolean ProdConfigName{get;set;}
        @AuraEnabled
        public String ProdBasketId{get;set;}
        @AuraEnabled
        public String Type{get;set;}
        @AuraEnabled
        public String NumberRange{get;set;}
        @AuraEnabled
        public String Status{get;set;}
    }
    
    //Wrapper to pass UI input values
    public class SearchReservedNumberWrapper {
        @AuraEnabled
        public String selectedTabId{get;set;}
        @AuraEnabled
        public String basket_id{get;set;}
        @AuraEnabled
        public List<String> configId{get;set;}
    }
/*-------------------------------------------------------- 
Method: getNumberList
Description: to get the list of reserved Number__C for given basketID and Product Configs selected.
Author:Sandip Deshmane 
--------------------------------------------------------*/
    @AuraEnabled
    public static List<NumberWrapper> getNumberList(SearchReservedNumberWrapper searchObj) {
        
        List<String> servList = new List<String>();
        Map<String,string> startRange = new Map<String,string>();
        String basketId = searchObj.basket_id;
        String selectedTab = searchObj.selectedTabId;
        List<String> configId = searchObj.configId;
        system.debug('Configids+' +configId);
        system.debug('basketId+' +basketId);
        system.debug('selectedTab+' +selectedTab);
         List< NumberWrapper> lstNumWrap= new List<NumberWrapper>();
        
        system.debug('servList+' +servList);
        /*if(pconfigList!=NULL && pconfigList.size()>0){
            
            system.debug('Service Type+' +selectedTab);
            system.debug('Basket Id+' +basketId);
            List<Number__C> lstNumber=[select Id, Type__c,Mobile__c, Service_Id__c, Service_Number__c,Product_Configuration__c, Product_Configuration__r.Name,Basket__c,Start_Range__c,End_range__c,Number_Range__c,Status__c from Number__c where Mobile__c= :selectedTab and Service_Id__c IN :servList and Status__c = : STATUS_ACTIVE];
            system.debug('lstNumber+' +lstNumber);
            if(lstNumber!=null && lstNumber.size()>0){
                
                for(integer i=0; i<lstNumber.size();i++){
                    NumberWrapper numWrapObj= new NumberWrapper();
                     NumberWrapper detailList = new NumberWrapper();
                     numWrapObj.PhoneNumber=lstNumber[i].Service_Number__c;
                     detailList= getDetails(lstNumber[i],numWrapObj);
                     lstNumWrap.add(detailList);
                     system.debug('lstNumWrap+' +lstNumWrap);
                }
        
            }
        }*/
        system.debug('lstNumWrap+' +lstNumWrap);
        return lstNumWrap;
    }
/*-------------------------------------------------------- 
Method: getDetails
Description: Populating NumberWrapper by passing from Number Object
Author:Sandip Deshmane 
--------------------------------------------------------*/
   /* @AuraEnabled
    public static NumberWrapper getDetails(Number__C num, NumberWrapper numWrapObj){
        if(num!=null){
                numWrapObj.numberId=num.id;
                numWrapObj.Type=num.Type__c;
                    numWrapObj.IsSelected= num.Product_Configuration__c!=null? true: false;                
                    numWrapObj.ProdConfigId=num.Product_Configuration__c;  
                    numWrapObj.ProdConfigName=num.Product_Configuration__c!=null? true: false;
                    numWrapObj.ProdBasketId=num.Basket__c;
                numWrapObj.NumberRange = num.Number_Range__c;
                    numWrapObj.Status = num.Status__c;
        }
        return numWrapObj;
    }*/
/*************************************************************************************************
Name : removeReservedNumbers
Description : Update status of selected Number to Pending Disconnection
Author: Sandip Deshmane
***************************************************************************************************/
    @AuraEnabled
     public static List<NumberWrapper> removeReservedNumbers(String selectedNumList,String removalPoolList, String basketid){
        System.debug('in remove');
         List<NumberWrapper> selectedNums = new List<NumberWrapper>();
        List<NumberWrapper> removalPoolNums = new List<NumberWrapper>();
        List<NumberWrapper> lstNumWrap= new List<NumberWrapper>();
        if(String.isNotBlank(selectedNumList))
        selectedNums = (List<NumberWrapper>)System.JSON.deserialize(selectedNumList, List<NumberWrapper>.Class);
        if(String.isNotBlank(removalPoolList))
        removalPoolNums = (List<NumberWrapper>)System.JSON.deserialize(removalPoolList, List<NumberWrapper>.Class);
        Map<Id,NumberWrapper> numPoolMap = new Map<Id,NumberWrapper>();
         for(NumberWrapper num : removalPoolNums){
             numPoolMap.put(num.numberId, num);
         }
         List<String> serviceNumber = new List<String>();
         //List<Number__c> updatedNums = new List<Number__c>();
         for(NumberWrapper selectedNum : selectedNums){
             serviceNumber.add(selectedNum.PhoneNumber);
         }
         
        /*if(!selectedNums.isEmpty()){
        List<Number__c> numList = [SELECT ID,Type__c,Mobile__c, Service_Id__c, Service_Number__c,Product_Configuration__c, Product_Configuration__r.Name,Basket__c,Start_Range__c,End_range__c,Number_Range__c,Status__c FROM Number__c WHERE Basket__c = :basketid AND Service_Number__c IN :serviceNumber];
        System.debug('numList: '+numList);
            try{
            for(Number__c num: numList){
                num.Status__c = NUMBER_STATUS;
                if(numPoolMap.containsKey(num.ID)){
                    NumberWrapper numWrapObj= new NumberWrapper();
                    NumberWrapper updatedNumber = new NumberWrapper();
                    numWrapObj.PhoneNumber=num.Service_Number__c;
                    updatedNumber= getDetails(num,numWrapObj);
                    system.debug('updatedNumber+' +updatedNumber);
                    numPoolMap.put(num.ID, updatedNumber);
                }
                
                updatedNums.add(num);
            }
            
            if(!updatedNums.isEmpty()){
                update updatedNums; 
            }
            for(NumberWrapper numWrap : numPoolMap.values()){
              lstNumWrap.add(numWrap);
            }
                
        }
        catch(DmlException de){
            System.debug('DmlException:'+de.getStackTraceString());
        }
     }*/
         return lstNumWrap;
     }
/*-------------------------------------------------------- 
Method: addToNumberPool
Description: Add selected number to Number pool and remove from Existing numbers.
Author:Sandip Deshmane 
--------------------------------------------------------*/
    @AuraEnabled
         public static Map<String, List<NumberWrapper>> addToNumberPool(String selectedNumList, String existingNumList, String reservedNumList){
           List<NumberWrapper> existingNums = new List<NumberWrapper>();
            List<NumberWrapper> selectedNums = new List<NumberWrapper>();
            List<NumberWrapper> reservedNums = new List<NumberWrapper>();
            List<NumberWrapper> newReservedNums = new List<NumberWrapper>();
            
            if(String.isNotBlank(selectedNumList))
            selectedNums = (List<NumberWrapper>)System.JSON.deserialize(selectedNumList, List<NumberWrapper>.Class);
            if(String.isNotBlank(existingNumList))
            existingNums = (List<NumberWrapper>)System.JSON.deserialize(existingNumList, List<NumberWrapper>.Class);
            if(String.isNotBlank(reservedNumList))
            reservedNums = (List<NumberWrapper>)System.JSON.deserialize(reservedNumList, List<NumberWrapper>.Class);
            
             if(selectedNums != null){
                 for(NumberWrapper newNumber : selectedNums){
                   existingNums.add(newNumber);
               }
             }
             Map<String, NumberWrapper> reservedNumMap = new Map<String, NumberWrapper>();
             Map<String, List<NumberWrapper>> allNumMap = new Map<String, List<NumberWrapper>>();
             for(NumberWrapper reservedNum : reservedNums){
                 reservedNumMap.put(reservedNum.numberId, reservedNum);
             }
             for(NumberWrapper existingNum : existingNums){
                 if(reservedNumMap.containsKey(existingNum.numberId)){
                     reservedNumMap.remove(existingNum.numberId);
                 }
             }
             
             for(NumberWrapper remNumber : reservedNumMap.values()){
                 newReservedNums.add(remNumber);
             }
             allNumMap.put('NumbersToRemove', existingNums);
             allNumMap.put('NumbersRemaining', newReservedNums);    
             return allNumMap;
         }
     
/*-------------------------------------------------------- 
Method: removeFromNumberPool
Description: Remove selected Numbers from Number Pool and add it to ExistingNumbers
Author:Sandip Deshmane 
--------------------------------------------------------*/
    @AuraEnabled
         public static Map<String, List<NumberWrapper>> removeFromNumberPool(String selectedNumList, String removalPoolList, String reservedNumList){
           List<NumberWrapper> removalPool = new List<NumberWrapper>();
            List<NumberWrapper> selectedNums = new List<NumberWrapper>();
            List<NumberWrapper> reservedNums = new List<NumberWrapper>();
            List<NumberWrapper> newRemovalNums = new List<NumberWrapper>();
            
            if(String.isNotBlank(selectedNumList))
            selectedNums = (List<NumberWrapper>)System.JSON.deserialize(selectedNumList, List<NumberWrapper>.Class);
            if(String.isNotBlank(removalPoolList))
            removalPool = (List<NumberWrapper>)System.JSON.deserialize(removalPoolList, List<NumberWrapper>.Class);
            if(String.isNotBlank(reservedNumList))
            reservedNums = (List<NumberWrapper>)System.JSON.deserialize(reservedNumList, List<NumberWrapper>.Class);
            
             if(selectedNums != null){
                 for(NumberWrapper newNumber : selectedNums){
                   reservedNums.add(newNumber);
               }
             }
             Map<String, NumberWrapper> removalPoolMap = new Map<String, NumberWrapper>();
             Map<String, List<NumberWrapper>> allNumMap = new Map<String, List<NumberWrapper>>();
             for(NumberWrapper removalNum : removalPool){
                 removalPoolMap.put(removalNum.numberId, removalNum);
             }
             for(NumberWrapper selectedNum : selectedNums){
                 if(removalPoolMap.containsKey(selectedNum.numberId)){
                     removalPoolMap.remove(selectedNum.numberId);
                 }
             }
             
             for(NumberWrapper remNumber : removalPoolMap.values()){
                 newRemovalNums.add(remNumber);
             }
             allNumMap.put('NumbersReserved', reservedNums);
             allNumMap.put('NumbersRemaining', newRemovalNums);    
             return allNumMap;
         }
}