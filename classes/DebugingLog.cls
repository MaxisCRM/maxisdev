public class DebugingLog {
    public virtual class Log{
    public String Type;
    public String ApexClass;
    public String Method;
    public String RecordId;
    public String Message;
    public String StackTrace;
    }
    
   	public class Error extends Log{
        public Error(String cls, String routine, String recId, Exception ex){
    		this.Type = 'Error';
            this.ApexClass = cls;
            this.Method = routine;
            this.RecordId = recId;
            this.Message = ex.getMessage();
            this.StackTrace = ex.getStackTraceString();
        }
    }
    
   public class Information extends Log{
       public Information(String cls, String routine, String recId, String msg){
           this.Type = 'Information';
           this.ApexClass = cls;
           this.Method = routine;
           this.RecordId = recId;
           this.Message = msg;
           this.StackTrace = NULL;
        }
    }
    
    public void createLog(Log logToCreate){
        try{
            if(
                (Limits.getDMLRows() < Limits.getLimitDMLRows()) && 
                (Limits.getDMLStatements() < Limits.getLimitDMLStatements())
            )
            {
                Log__c apexDebuglog = new Log__c(
                    Exception_Type__c       = logToCreate.Type,
                    Class__c  				= logToCreate.ApexClass,
                    Method__c       		= logToCreate.Method,
                    Records__c	    		= logToCreate.RecordId,
                    Message__c      		= logToCreate.Message,
                    Stack_Trace__c  		= logToCreate.StackTrace
                );

                Database.insert(apexDebuglog, FALSE);
            }
            else{
                System.debug('The Governor Limits have already been exhausted and hence failed to create a Log!');
            }
        }
        catch(DMLException ex){
            System.debug('Something fatal has occurred and hence failed to create a Log! Error:' + ex.getMessage());
        }
    }
}