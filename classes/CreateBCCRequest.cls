Global with sharing class CreateBCCRequest implements CSPOFA.ExecutionHandler{
    public List<SObject> process(List<SObject> steps)
    {
        List<CSPOFA__Orchestration_Step__c> result = new List<CSPOFA__Orchestration_Step__c>();
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)steps;
        Set<String> brnSet = new Set<String>();
        Set<Id> orchesProcessIdSet= new Set<Id>();
        
        for(CSPOFA__Orchestration_Step__c step: stepList)
        {
            orchesProcessIdSet.add(step.CSPOFA__Orchestration_Process__c);
        }
        system.debug('orchesProcessIdSet ::>'+orchesProcessIdSet);
        for(CSPOFA__Orchestration_Process__c  orch : [Select Id, CSPOFA__Account__r.BRN__c from CSPOFA__Orchestration_Process__c where Id IN:orchesProcessIdSet])
        {
            brnSet.add(orch.CSPOFA__Account__r.BRN__c);
        }       
        
        if(brnSet.isEmpty())
        {
                List<Integration_endpoints__c> endpointList = Integration_endpoints__c.getAll().values();
                for(String brnNumber : brnSet)
                {
                    if (Limits.getCallouts() < Limits.getLimitCallouts() && (brnNumber != null && brnNumber != '') && (!endpointList.isEmpty() && endpointList[0].BCC_Endpoint__c != null && endpointList[0].BCC_Endpoint__c !=  '')) 
                    {
                        //List<Integration_endpoints__c> endpointList = Integration_endpoints__c.getAll().values();
                        HttpRequest req = new HttpRequest();
                        HttpResponse res = new HttpResponse();
                        Http http = new Http();

                        req.setEndpoint(endpointList[0].BCC_Endpoint__c);
                        req.setMethod('POST');                        
                        
                        req.setHeader('Content-Type', 'application/json');
                        req.setHeader('x-source- countrycode', 'MY');
                        req.setHeader('x-source-channel', 'CPQ');
                        req.setHeader('x-source- channelrefid', 'UUID');
                        req.setHeader('x-source- correlationid', 'UUID');
                        req.setHeader('x-source-timestamp', 'yyyyMMddHHmmss');

                        JSONGenerator gen = JSON.createGenerator(true);    
                        gen.writeStartObject();      
                        gen.writeStringField('dealerId', '123487664');
                        gen.writeStringField('eRFId', '123487664');
                        gen.writeStringField('brnNo', brnNumber);
                        gen.writeStringField('registrationType', '123487664');
                        gen.writeStringField('typeOfAccount', '123487664');
                        gen.writeStringField('typeOfCustomer', '123487664');
                        gen.writeStringField('typeOfCompany', '123487664');
                        gen.writeStringField('companyName', '123487664');
                        gen.writeStringField('natureOfBusiness', '123487664');
                        gen.writeStringField('noOfEmployee', '123487664');
                        gen.writeStringField('annualSalesTurnover', '123487664');
                        gen.writeStringField('donor', '123487664');
                        gen.writeStringField('billAddresss1', '123487664');
                        gen.writeStringField('billAddresss2', '123487664');
                        gen.writeStringField('billCity', '123487664');
                        gen.writeStringField('billPostcode', '123487664');
                        gen.writeStringField('billState', '123487664');
                        gen.writeStringField('billCountry', '123487664');
                        gen.writeStringField('installationAddresss1', '123487664');
                        gen.writeStringField('installationAddresss2', '123487664');
                        gen.writeStringField('installationCity', '123487664');
                        gen.writeStringField('installationPostcode', '123487664');
                        gen.writeStringField('installationState', '123487664');
                        gen.writeStringField('installationCountry', '123487664');
                        gen.writeStringField('typeOfAccount', '123487664');
                        gen.writeStringField('typeOfCustomer', '123487664');
                        gen.writeStringField('typeOfCompany', '123487664');
                        gen.writeStringField('companyName', '123487664');
                        gen.writeEndObject();    
                        String body = gen.getAsString();  
                        req.setbody(body);                      
                        
                        res = http.send(req);
                        if(res.getStatusCode() == 201)
                        {
                           //Requested created succesfully
                        }
                        else
                        {
                            //Error in request creation
                        }
                    }

                }           

        }
        return result;
    }    
}