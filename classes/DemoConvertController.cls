global class DemoConvertController extends csbb.CustomButtonExt {
    
    global String performAction (String basketId) {
        //csdg.DocGenerationWrapper result;
        String newUrl = '';
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, String> constantsMap = new Map<String, String>();
        //try{
            inputMap.put('recordId', basketId);
            csdg__Data_Source__c constantsDS = [SELECT Id FROM csdg__Data_Source__c WHERE Name = 'Fixed Comm. Basket' Limit 1];
            inputMap.put(constantsDS.Id, constantsMap);
            //inputMap.put('userId', System.UserInfo.getUserId());
            //result = csdg.DocumentDataHandler.generate('a3M0T0000004rIA',basketId , inputMap);
            String result = csdg.ConvertController.convertToPdf('a3M0T0000004rIA', basketId);
            system.debug('result =====' + result );
            newUrl = '/servlet/servlet.FileDownload?file=' + result;
            
        /*}      
        catch(Exception e){
            system.debug('exception:::' + e.getLineNumber() + '====' + e.getMessage());
        }*/
        return '{"status":"ok","redirectURL":"' + newUrl + '","text":"Loading Service Order Page"}';
    }
}