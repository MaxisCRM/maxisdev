public class ProductBasketHandler {
    public static void beforeInsert(List<cscfga__Product_Basket__c> newProductBasketList){
        List<Id> oppIds = new List<Id>();
        for(cscfga__Product_Basket__c productBasket: newProductBasketList){
            oppIds.add(productBasket.cscfga__Opportunity__c);
        }
        
        Map<Id,Opportunity> mapOfOpportunity = new Map<Id,Opportunity>([select id, AccountId from opportunity where id in:oppIds]);
            
        for(cscfga__Product_Basket__c productBasket: newProductBasketList){
            productBasket.csbb__Account__c  = mapOfOpportunity.get(productBasket.cscfga__Opportunity__c).AccountId;
        }
     }
}