public class NumberReservationController {

    @auraEnabled
    public static List<postCodeCity> prePopulateValues(String basketId,String defaultSite){
        List<postCodeCity> listOfPostCodeWrapper = new List<postCodeCity>();
        /*cscfga__Product_Basket__c productBasket = [select cscfga__Opportunity__r.AccountId from cscfga__Product_Basket__c where Id =:basketId];
        for(cscrm__Site__c siteAddress : [select cscrm__Installation_Address__c,cscrm__Installation_Address__r.cscrm__Zip_Postal_Code__c,cscrm__Installation_Address__r.cscrm__City__c from cscrm__Site__c where cscrm__Account__c =:productBasket.cscfga__Opportunity__r.AccountId and cscrm__Installation_Address__c != null]){
            listOfPostCodes.add(siteAddress.cscrm__Installation_Address__r.cscrm__Zip_Postal_Code__c);
            
        }*/
        for(cscrm__Site__c siteAddress : [select cscrm__Installation_Address__c,cscrm__Installation_Address__r.cscrm__Zip_Postal_Code__c,cscrm__Installation_Address__r.cscrm__City__c from cscrm__Site__c where Id =:defaultSite and cscrm__Installation_Address__c != null]){
            listOfPostCodeWrapper.add(new postCodeCity(siteAddress.cscrm__Installation_Address__r.cscrm__Zip_Postal_Code__c,siteAddress.cscrm__Installation_Address__r.cscrm__City__c));
            
        }
        return listOfPostCodeWrapper;
    }
    
    @auraEnabled
    public static List<availableNumber> searchAvailableNumbers(String postCode,String city,Integer quantity){
        List<availableNumber> availableNumbers = new List<availableNumber>();
        // logic to integrate with api and get numbers
        if(quantity == null || quantity <0){
        	throw new AuraHandledException('Please enter a valid quantity');
    	}
        for(Integer i=0;i<quantity;i++){
        	availableNumbers.add(new availableNumber('42554656'+i));
    	}
        return availableNumbers;
    }
    @auraEnabled
    public static Map<Id,String> assignNumbers(List<Id> voiceIds, String jsonList){
        List<availableNumber> availableNumbers = (List<availableNumber>)JSON.deserialize(jsonList,List<availableNumber>.class);
        Map<Id,String> voiceToNumber = new Map<Id,String>();
        if(voiceIds.isEmpty() || availableNumbers.isEmpty()){
            //error
            throw new AuraHandledException('Please select one or more voices/available numbers to proceed');
        }
        else if(voiceIds.size() != availableNumbers.size()){
            //error
            throw new AuraHandledException('Please select equal number of voices/available numbers to proceed');
        }
        else if(voiceIds.size() == availableNumbers.size()){
            Integer i=0;
            for(Id voiceId :voiceIds){
                voiceToNumber.put(voiceId, availableNumbers[i].availNumber);
                i++;
            }
        }
        return voiceToNumber;
    }
    public static void reserveSelected(String voiceListJSON){
        List<voiceItemWrapper> voiceList = (List<voiceItemWrapper>)JSON.deserialize(voiceListJSON,List<voiceItemWrapper>.class);
        
        // Query required Schema
            List<csoe__Non_Commercial_Schema__c > ncs = [
                select id
                from csoe__Non_Commercial_Schema__c 
                where name = 'Number Reservation'
            ];
             
            // Create list of configurations. GUID is optional. Not
            // all attributes are required. Only those which should
            // have values set.
            List<Object> configList = new List<Object>();
        	for(voiceItemWrapper voiceItem :voiceList){
                Map<String, String> config = new Map<String, String>();
                config.put('Id', voiceItem.voiceId);
                config.put('Pilot', voiceItem.isPilot?'true':'false');
                config.put('Hunting', voiceItem.isHunting?'true':'false');
                config.put('msisdnNumber', voiceItem.msisdnNumber);
                configList.add((Object) config);
    		}
            // Insert configs for queried schema (create non
            // commercial entity)
            Map<String,Object> nce = new Map<String,Object>();
            nce.put('name', (Object) 'Number Reservation');
            nce.put('id', (Object) ncs[0].id);
            nce.put('configurations', configList);
             
            List<Object> nceList = new List<Object>();
            nceList.add((Object) nce);
             
            Map<Id,List<Object>> oeMap = new Map<Id, List<Object>>();
            oeMap.put(ncs[0].id, nceList);
             
            csoe.API_1.createOEData(oeMap);
    }
    
    public class availableNumber{
        @auraEnabled
        public string availNumber {get;set;}
        
        public availableNumber(String inNumber){
            availNumber = inNumber;
        }
    }
    
    public class postCodeCity{
        @auraEnabled
        public string postCode {get;set;}
        @auraEnabled
        public string cityName {get;set;}
        
        public postCodeCity(String postCode,String cityName){
            this.postCode = postCode;
            this.cityName = cityName;
        }
    }
    
    public class voiceItemWrapper{
        @auraEnabled
        public string vName {get;set;}
        @auraEnabled
        public string msisdnNumber {get;set;}
        @auraEnabled
        public boolean isPilot {get;set;}
        @auraEnabled
        public boolean isHunting {get;set;}
        @auraEnabled
        public string voiceId {get;set;}
        
        public voiceItemWrapper(){
            vName = '';
            msisdnNumber = '';
            isPilot = false;
            isHunting = false;
            voiceId = null;
        }
    }
}