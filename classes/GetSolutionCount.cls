global class GetSolutionCount implements cssmgnt.RemoteActionDataProvider {
    @RemoteAction
    global static Map<String,Object> getData(Map<String,Object> inputMap) {
        Map<String, Object> returnMap = new Map<String, Object>();   
        List<csord__Solution__c> solList = new List<csord__Solution__c>();
        String basketId = (String)inputMap.get('basketId');
        Integer BVECount = 0;
        Integer otherCount = 0;
        Boolean status = false;
        solList = [SELECT id,Name FROM csord__Solution__c WHERE cssdm__product_basket__c =:basketId];
        if(solList != null && !solList.isEmpty()){
            for(csord__Solution__c sol : solList){
                if(sol.Name.contains('Business Voice Enhanced Solution')){
                    BVECount++;
                }
                else if(!sol.Name.contains('Business Voice Enhanced Solution')){
                    otherCount++;
                }
            }
            if(BVECount > 0 && otherCount == 0){
                status = false; 
                returnMap.put('Status', status);
            }
            else if(BVECount > 0 && otherCount > 0){
                status = true; 
                returnMap.put('Status', status);
            }
        }
        return returnMap;
    }
}