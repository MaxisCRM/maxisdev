global with sharing class ConnectivityServicePriceItemLookup extends cscfga.ALookupSearch {
    public override String getRequiredAttributes() {
        return '["PDName","PriceItemId","slastr"]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {

        system.debug(LoggingLevel.ERROR, 'CommercialProductLookup: ' + JSON.serialize(searchFields));

        String pdName = searchFields.get('PDName') != null ? searchFields.get('PDName') : null;
        String PriceItem = searchFields.get('PriceItemId') != null ? searchFields.get('PriceItemId') : null ;
         String priceitemstring = searchFields.get('slastr') != null && !String.isBlank(searchFields.get('slastr'))? searchFields.get('slastr'):null;
         //system.debug(priceitemstring.split('\\('));
        List<String> priceitemstringlst = !String.isBlank(searchFields.get('slastr')) && priceitemstring.contains('(') ? priceitemstring.split('\\('):!String.isBlank(searchFields.get('slastr')) && !priceitemstring.contains('(') ? new List<String>{priceitemstring} : null ;
        
        priceitemstring = priceitemstringlst != null ? '%'+priceitemstringlst[0]+'%' : null;
        system.debug(LoggingLevel.ERROR, 'CommercialProductLookup PDName ' + PDName);
 
       /* List<cspmb__Price_Item__c> pItems = [
            select Id, Name, Service_Type__c, Bandwidth__c, cspmb__Contract_Term__c
            from cspmb__Price_Item__c
            where cspmb__Product_Definition_Name__c = :pdName and Service_Type__c != null order by Name asc
        ];
        */
        String Query = 'select Id, Name, Service_Type__c, Bandwidth__c, cspmb__Contract_Term__c, Managed_Router__c, cspmb__One_Off_Charge__c, cspmb__Recurring_Charge__c, PriceMarkup_Percentage__c from cspmb__Price_Item__c where cspmb__Product_Definition_Name__c = :pdName and Service_Type__c != null ';
        if(PriceItem != null &&  priceitemstring != null && !String.isBlank(priceitemstring)){
        Query += ' and name like :priceitemstring ';
        }
        
        Query += ' order by Name asc ';
        
         List<cspmb__Price_Item__c> pItems = Database.query(Query);
         
         List<cspmb__Price_Item__c> returnlst = new List<cspmb__Price_Item__c>();
         if(PriceItem != null &&  priceitemstring != null && !String.isBlank(priceitemstring)){
           returnlst=  getUniqueListByField(pItems,'Managed_Router__c');
         }
         else {
           returnlst =  getUniqueListByField(pItems,'Name');
         }
         system.debug('%returnmap%'+returnlst);
         return returnlst;
        // get distinct list
        
    }
    public List<cspmb__Price_Item__c> getUniqueListByField (List<cspmb__Price_Item__c> pItems,String field){
        Map<String,cspmb__Price_Item__c> pItemsMap = new  Map<String,cspmb__Price_Item__c>();
        if(pItems!=null)
        {
            for(cspmb__Price_Item__c item : pItems){
                if(field == 'Name' && !pItemsMap.containskey(item.Name)){
                system.debug('field'+item);
                pItemsMap.put(item.Name,item);
                }
                else if(field == 'Managed_Router__c' && !pItemsMap.containskey(item.Managed_Router__c)){
                 pItemsMap.put(item.Managed_Router__c,item);
                }
            }
        } 
        if(pItemsMap!=null && pItemsMap.size()>0)  {
        system.debug('pItemsMap'+pItemsMap);     
          return pItemsMap.Values();
          }
        else
          return null;  
    }
}