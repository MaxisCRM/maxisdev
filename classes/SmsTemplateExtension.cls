public class SmsTemplateExtension 
{
	private final Sms_Log__c thisCase;
    private final Sms_Log__c smsLog;
    private final Sms_Log__c recordLog;
    private final Account AccountId;
    public List<QuickText> textLst {get; set;}
    public List<WrapQuickText> WrapTextList {get; set;}
    public List<QuickText> selectedTemplate{get;set;}
    public boolean displayPopup {get;set;} 
    //public ApexPages.StandardSetController stdCntrlr {get; set;} 

    
    public SmsTemplateExtension(ApexPages.StandardSetController stdController) 
    {
        AccountId = [Select Name, BRN_Type__c, (SELECT id FROM Contacts )  From Account Where Id =:ApexPages.currentPage().getParameters().get('Id')];
        System.debug(AccountId);
    	textLst = [Select Name, Message From QuickText Where Category = 'SMS Templates'];
        if(WrapTextList == null) 
        {
            WrapTextList = new List<WrapQuickText>();
            for(QuickText qt: [select Id, Name, Message from QuickText Where Category = 'SMS Templates']) 
            {
                WrapTextList.add(new WrapQuickText(qt));
            }
         }
    }
    
    public PageReference cancel ()
    {
        PageReference accountPage = new ApexPages.StandardController(AccountId).view();
        accountPage.setRedirect(true);
        return accountPage;
    }
    
    public pagereference save()
    {
        String SMS_content;
        for(WrapQuickText a : WrapTextList) 
        {
            if(a.selected == true) 
            {
               SMS_content = a.qtTemp.Message;
               System.Debug('from wrap text: ' + SMS_content);
            }
        }
        Sms_Log__c smsCreation = new Sms_Log__c( RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Sms_Log__c' 
                                                 AND DeveloperName = 'testtet' Limit 1].Id, Account__c=AccountId.ID, SMS_Content__c=SMS_content);
	   
        insert smsCreation;
        return new PageReference ('/' + smsCreation.Id);
    }
    
    public void processSelected() 
    {
    	selectedTemplate = new List<QuickText>();
        for(WrapQuickText a : WrapTextList) 
        {
            if(a.selected == true) 
            {
                smsLog.SMS_Content__c = a.qtTemp.Message;
            }
        }
    }
    
    public class WrapQuickText 
    {
        public QuickText qtTemp {get; set;}
        public Boolean selected {get; set;}
 
        public WrapQuickText(QuickText qt) 
        {
            qtTemp = qt;
            selected = false;
        }
    }
    /*
    public void showTemplateList() 
    { displayPopup = true; } 
    public void dontshowTemplateList() 
    { displayPopup = false; } */
}