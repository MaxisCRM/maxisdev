/**
* File Name:      LeadBulkImportTriggerController.apxc
* Author:         ACN Janice Fong
* Created Date:   19/12/2019
* Description:    Lead Trigger Controller 
                  Bulk Import 
                  - assign the lead to the account owner when the Lead's Company = Account Name
                  - else assign the lead to SME queue
                  Account Manager Insertion
                  - Account Manager can create leads where they own the Lead
**/

public with sharing class LeadBulkImportTriggerController {
   /**
   *Method Name:  checkOwner
   *Description:  a method to assign the lead to the account owner when the Lead's Company = Account Name
                  else assign the lead to SME queue
   **/
   public void checkOwner(List<Lead> leadList){
      //get User profile
      List<User> listUser = [SELECT Name, Profile.Name from User WHERE Id =: UserInfo.getUserId() LIMIT 1];
      
      //Remove abbrevation in company name (e.g. sdn bhd)
      //List<Company_Abbrevation__mdt> abbList = [SELECT MasterLabel from Company_Abbrevation__mdt];
      if(listUser.size()>0){
         if(listUser[0].Profile.Name == 'System Administrator' || listUser[0].Profile.Name == 'Sales Admin'){ 

            Map<String,Account> mapAcc = new Map<String,Account>();
            for(Account acc: [SELECT Id,Name,OwnerId,Segment__c FROM Account where not segment__c LIKE '%SME%']){
               mapAcc.put(acc.Name.toUpperCase(), acc);
            }

            //get SME queue id
            List<Group> listQueue = [SELECT Id,DeveloperName,Name,Type FROM Group where type = 'Queue' AND DeveloperName = 'SME_Queue' LIMIT 1]; 

            for(Lead lead: leadList){
               if(lead.OwnerId == UserInfo.getUserId()){
                   Id idOwner = null;
                   if(lead.Company != null || !String.isEmpty(lead.Company)){
                      String strName = '';
                      if(mapAcc.get(lead.Company.toUpperCase()) != null ){
                         Account acc = mapAcc.get(lead.Company.toUpperCase());
                         idOwner = acc.ownerId;
                         strName = acc.name;
                      }
                      /*else{
                         for(Account acc:accList){
                            double match = 0;
                            double num = FuzzyAlgorithmClass.executeFuzzyLogic(abbList,lead.Company,acc.Name);
                            //double num = FuzzyAlgorithmClass.executeFuzzyLogic(lead.Company,acc.Name);
                            if(num>match){
                               match = num;
                               ownerId = acc.ownerId;
                               name = acc.name;
                            }
                         }
                      }*/
                   }
                   if(idOwner == null){
                      lead.ownerID = listQueue[0].id;
                   }else{
                      lead.ownerID = idOwner;
                   }
                   system.debug('Lead '+ lead.company);
                   system.debug('Lead ' + lead.ownerId);
               }
            }
         }
      }
   }
}