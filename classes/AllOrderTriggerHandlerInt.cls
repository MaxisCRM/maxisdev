public without sharing class AllOrderTriggerHandlerInt extends TriggerHandlerBaseInt{

    List<CSPOFA__Orchestration_Process__c> toInsertOrcheProcesList = new List<CSPOFA__Orchestration_Process__c>();
    CSPOFA__Orchestration_Process_Template__c orderProviOrcheProcessTemplate = new CSPOFA__Orchestration_Process_Template__c();
    /*
    * This method is called prior to execution of a AFTER trigger. Use this to cache
    * any data required into maps prior execution of the trigger.
    */    
    public override void bulkAfter() {
        if(trigger.isInsert)
        {
            orderProviOrcheProcessTemplate = [SELECT Id FROM CSPOFA__Orchestration_Process_Template__c WHERE CSPOFA__Unique_Name__c = 'Order Provisioning Template' Limit 1];      
            System.debug('orderProviOrcheProcessTemplate ::>'+orderProviOrcheProcessTemplate);
        }
    }
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    public override void afterInsert(SObject so)
    {
        CSPOFA__Orchestration_Process__c orchestrationProcessRec = AllOrderTriggerHelperInt.initiateOrderOrchestrationHelper((csord__Order__c)so, orderProviOrcheProcessTemplate);
        System.debug('orchestrationProcessRec ::>'+orchestrationProcessRec);
        if(orchestrationProcessRec != null)
        {
            toInsertOrcheProcesList.add(orchestrationProcessRec);
        }
    }

    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this 
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public override void andFinally() {
        System.debug('toInsertOrcheProcesList ::>'+toInsertOrcheProcesList);
        if(!toInsertOrcheProcesList.isEmpty())
        {
            insert toInsertOrcheProcesList;
            toInsertOrcheProcesList.clear();
        }
    }
}