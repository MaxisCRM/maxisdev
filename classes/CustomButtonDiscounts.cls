global with sharing class CustomButtonDiscounts extends csbb.CustomButtonExt {
    public String performAction (String basketId) {
        String newUrl = '/apex/csDiscounts__DiscountPage?basketId=' + basketId;
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
    }
}