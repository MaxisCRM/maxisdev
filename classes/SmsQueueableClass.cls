public class SmsQueueableClass implements Queueable {
	private SMS_Log__c smsLog
    {
        get;set;
    }
    public SmsQueueableClass(SMS_Log__c smsLog)
    {
        this.smsLog = smsLog;
    }
    public void execute(QueueableContext context)
    {
        List<SMS_Log__c> SmsLogList = New List<SMS_Log__c> ([SELECT Id, Mobile_Number__c, SMS_Content__c
                                           FROM SMS_Log__c
                                           WHERE Id =: this.smsLog.Id LIMIT 1]);
        
        if (!String.isBlank(SmsLogList[0].SMS_Content__c) ) 
        {
            SmsLogList[0].Total_Message_Count__c = SmsLogList[0].SMS_Content__c.length();
        }
        
     //   SmsCalloutClass.sendSMStoExternalSystem(SmsLogList[0]);
    }
}