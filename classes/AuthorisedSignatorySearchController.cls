public with sharing class AuthorisedSignatorySearchController {
public static Map<String,Contactwrap> contactwrapmap = new Map<String,Contactwrap>();
    @AuraEnabled
    public static List<Contactwrap> searchcontacts(String accid){
        system.debug('accid'+accid);
        List<Contactwrap> contactwrapperlst = new List<Contactwrap>();
        for(Contact c: [select id, Authorized_Signatory__c ,Name,Title from Contact where AccountId =:accid limit 100]){
            contactwrapperlst.add(new Contactwrap(c));
        }
		//contactwrapperlst.add(new Contactwrap('100','100','Venkat','Male','Manager','Active'));
        for(Contactwrap wrap : contactwrapperlst){
            contactwrapmap.put(wrap.RoleId,wrap);
        }
        //contactlst.add(a);
        return contactwrapperlst;
    }
    @AuraEnabled
    public static Boolean addSFsignatory(String accid,List<String> roleid,List<Object> contacts){
       // searchcontacts(accid);
        system.debug('accid'+accid);
        system.debug('roleid'+roleid);
         List<Contact> contactlst = new List<Contact>();
         //system.debug('contacts'+contacts[0]);
          for(String contid : roleid){
        for(Object con : contacts){
                String name = '';
                String designation = '';
                String rid = '';
                String rnum = '';
                String statuses = '';
                String gender = '';
            	String recid = '';
            Boolean issignatory ;
           Map<Object,Object> newmap = (Map<Object,Object>)con;
            for(Object key:newmap.keyset()){
                system.debug('newmapvalue'+Key+':'+newmap.get(key));
                 name = (key == 'Name') ? (String)newmap.get(key) : name;
                 designation = (key == 'Designation') ? (String)newmap.get(key) : designation;
                 rid = (key == 'RoleId') ? (String)newmap.get(key) : rid;
                rnum = (key == 'RoleNum') ? (String)newmap.get(key) : rnum;
                statuses = (key == 'Status') ? (String)newmap.get(key) : statuses;
                gender  = (key == 'Sex') ? (String)newmap.get(key) : gender;
                recid  = (key == 'recordId' && (String)newmap.get(key)!= '' && (String)newmap.get(key)!= null && !String.isBlank((String)newmap.get(key))) ? (String)newmap.get(key) : recid;
                issignatory  = (key == 'Signatory') ? (Boolean)newmap.get(key) : issignatory;
                if(key == 'RoleId' && (String)newmap.get(key)==contid){
                    system.debug('signatory contact new'+recid+' Name:'+name);
                     Contact c = new Contact();
            c.AccountId = accid;
            c.Authorized_Signatory__c = true;
            c.LastName = name;
			c.Phone = '8888';  
            c.id = /* (recid != '' && recid != null && !String.isBlank(recid)) ? */ recid;
            contactlst.add(c);            
            }
                if(key == 'Signatory' && (Boolean)newmap.get(key)){
 system.debug('signatory contact old'+recid+' Name:'+name);
                    Contact c = new Contact();
           
            c.Authorized_Signatory__c = false;
            
            c.id = /* (recid != '' && recid != null && !String.isBlank(recid)) ? */ recid;
            contactlst.add(c);            
            }
                }
           // system.debug('newmap.name'+newmap.get('Name'));
        }
        
          }
         system.debug('contactwrapmap'+contactwrapmap);
        system.debug('contactlst'+contactlst);
        Database.upsert(contactlst,false);
        return true;
    }
    @AuraEnabled
    public static Contact createSFcontact(String accid){
        system.debug('accid'+accid);
        Date d = Date.valueOf(System.now());
        List<Contact> contactlisttoupdate = new List<Contact>();
        Contact newsignatory = new Contact();
        Contact oldsignatory = new Contact();
        List<Contact> contactlst = [select id, Authorized_Signatory__c ,Name,Title,createdDate from Contact where AccountId =:accid and Authorized_Signatory__c = true order by createdDate desc limit 100];
        for (Integer i=0;i<contactlst.size();i++){
            
            newsignatory = i==0 && contactlst[i].createdDate > d? contactlst[i] : newsignatory;
            oldsignatory = i== 1 ? contactlst[i] : oldsignatory;
        }
         oldsignatory.Authorized_Signatory__c = false;
        contactlisttoupdate.add(oldsignatory);
        system.debug('contactlst'+contactlst);
        return contactlst[0];
    }

public class Contactwrap {
     @AuraEnabled public Id recordId;
        
        @AuraEnabled public String RoleNum ; 
    @AuraEnabled public String Name ; 
      @AuraEnabled public String Sex ; 
          @AuraEnabled public String Designation ; 
          @AuraEnabled public String Status ; 
    @AuraEnabled public Boolean Signatory;
      @AuraEnabled public String RoleId ;

  public Contactwrap() {
           
        }
        //This is the contructor method. When we create a new wrapAccount object we pass a Account that is set to the acc property. We also set the selected value to false
        public Contactwrap(Contact con) {
             recordId = con.Id;
            
            RoleNum = '1000';
            Name = con.Name;
            Sex = 'Male';
            Designation = con.Title;
            Status = 'Active';
            Signatory = con.Authorized_Signatory__c;
           RoleId = con.Id;
           // selected = false;
        }
    
    public Contactwrap(String id,String num,String cname,String gender,String title,String stat) {
           recordId = '';
            RoleNum = num;
            Name = cname;
            Sex = gender;
            Designation = title;
            Status = stat;
        Signatory = false;
        
         RoleId = id;
           // selected = false;
        }
    }
}