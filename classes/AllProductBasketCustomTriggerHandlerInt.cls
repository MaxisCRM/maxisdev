public class AllProductBasketCustomTriggerHandlerInt extends TriggerHandlerBaseInt{
    /*
    * This method is called prior to execution of a AFTER trigger. Use this to cache
    * any data required into maps prior execution of the trigger.
    */    
    public override void bulkAfter() 
    {

    }

    /*
    * This method is called prior to execution of a Before trigger. Use this to cache
    * any data required into maps prior execution of the trigger.
    */    
    public override void bulkBefore() 
    {
        
    }

     /* afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point. 
     * Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public override void afterInsert(SObject so)
    {
        csord__Order__c orderRec = (csord__Order__c)so;
    }
    
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public override void beforeInsert(SObject so) 
    {
        csord__Order__c orderRec = (csord__Order__c)so;
    }

    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    public override void afterUpdate(SObject oldSo, SObject so) 
    {
        csord__Order__c oldOrderRec = (csord__Order__c)oldSo;
        csord__Order__c newOrderRec = (csord__Order__c)so;
    }
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this 
     * method to accomplish any final operations such as creation or updates of other records.
     */
    public override void andFinally() 
    {

    }
}