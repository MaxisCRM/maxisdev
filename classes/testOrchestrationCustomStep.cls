Global with sharing class testOrchestrationCustomStep implements CSPOFA.ExecutionHandler{

    public List<SObject> process(List<SObject> steps){
        Set<Id> orchesProcessIdSet= new Set<Id>();        
        List<Account> accountDummyUpdateList = new List<account>();
        List<CSPOFA__Orchestration_Step__c> updateOPStepsList = new List<CSPOFA__Orchestration_Step__c>();
        for(CSPOFA__Orchestration_Step__c step: (List<CSPOFA__Orchestration_Step__c>)steps)
        {
            orchesProcessIdSet.add(step.CSPOFA__Orchestration_Process__c);
        }
        system.debug('orchesProcessIdSet ::>'+orchesProcessIdSet);
        for(CSPOFA__Orchestration_Process__c  orch : [Select Id, CSPOFA__Account__c from CSPOFA__Orchestration_Process__c where Id IN:orchesProcessIdSet])
        {
            Account accRec = new account(Id = orch.CSPOFA__Account__c);
            accRec.Description = accRec.Description + 'Lolla';
            accountDummyUpdateList.add(accRec);
        } 
        if(!accountDummyUpdateList.isEmpty())
        {
            update accountDummyUpdateList;
            for(CSPOFA__Orchestration_Step__c step: (List<CSPOFA__Orchestration_Step__c>)steps)
            {
                step.CSPOFA__Status__c = 'Complete';
                //check status before updating the following fields
                step.CSPOFA__Jeopardy_Created_Flag__c = false;
                step.CSPOFA__Message__c = 'Step 2 completed succesfully';
                step.CSPOFA__Completed_Date__c = Date.today();
                updateOPStepsList.add(step);
            }
            if(!updateOPStepsList.isEmpty())
            {
                 //update updateOPStepsList;
            }
        }
        return updateOPStepsList;
    }
   
}