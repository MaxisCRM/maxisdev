@isTest
public class Test_ProductBasketHandler {
    public static testmethod void testMethod1(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.BRN__c = '111';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.Name = 'Test 1';
        opp.StageName = 'Qualification';
        opp.CloseDate = Date.today().addDays(5);
        insert opp;
        
        cscfga__Product_Basket__c productBasket = new cscfga__Product_Basket__c();
        productBasket.cscfga__Opportunity__c = opp.Id;
        insert productBasket;
    }
}