/*
 * Class to reference the Trigger Settings custom setting
 *
 */
public with sharing class TriggerSettingsInt {

    private static GlobalMute__c orgTriggerSettings = GlobalMute__c.getOrgDefaults();
    private static GlobalMute__c userTriggerSettings = GlobalMute__c.getInstance(UserInfo.getUserId());
    private static List<string> objectsDisableList =    GlobalMute__c.getInstance(UserInfo.getUserId()).Objects_to_be_disabled__c.split(','); 
    public static Boolean isTriggerDisabled(SObjectType obType) {
        for(String objectName : objectsDisableList)
        {
            if(objectName.equalsIgnoreCase(obType.getDescribe().getName()))
            {
                return true;
            }
        }  
        return false;
    }

    public static Boolean isAllTriggerDisabled() {
        return orgTriggerSettings.AllTriggersDisabled__c || userTriggerSettings.AllTriggersDisabled__c;        
    }
}