public without sharing class AllOrderTriggerHelperInt {

    public static CSPOFA__Orchestration_Process__c initiateOrderOrchestrationHelper(csord__Order__c orderRec, CSPOFA__Orchestration_Process_Template__c orderProviOrcheProcessTemplate) {
        if(orderProviOrcheProcessTemplate != null && orderRec != null)
        {
            CSPOFA__Orchestration_Process__c orchesProcessRec = new CSPOFA__Orchestration_Process__c();
            orchesProcessRec.CSPOFA__Orchestration_Process_Template__c = orderProviOrcheProcessTemplate.Id;
            orchesProcessRec.CSPOFA__Account__c = orderRec.csord__Account__c;
            return orchesProcessRec;
        } 
        return null;
    }
}