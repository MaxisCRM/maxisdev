public with sharing class FileCalloutClass {
 public final String endPointUrl = 'http:// <http:///> <site url>';
 public final String accessToken = '';

 public void uploadFileToSharepoint(List<ContentDocument> docList){
  List<ContentDocument> contentDocumentList = [Select ContentAssetId,FileExtension,FileType,Id,ParentId,PublishStatus,SharingOption,SharingPrivacy,Title FROM ContentDocument where Id =: docList limit 500];
/*
  List<Id> docIdList = new List<Id>();
  for(ContentDocument doc : contentDocumentList){
   if(!docIdList.contains(doc.Id)){
    docIdList.add(doc.Id);
   }
  }

  List<ContentVersion> contentVersionList = this.getFile(docId);
*/
  for(ContentDocument doc : contentDocumentList){
   String fileName = doc.Title + '.'+doc.FileType;
   HttpRequest req = new HttpRequest();
   req.setEndpoint(endPointUrl+ '/_api/web/getfolderbyserverrelativeurl("/Shared Documents")/files add(overwrite=true,url='+fileName+')');
   req.setMethod('POST');
   req.setHeader('Authorization', 'Bearer ' +accessToken);
   req.setHeader('Accept','application/json;odata=verbose');
   Http http = new Http();
   HTTPResponse res = http.send(req);
   System.debug('*****Response*****'+res.getBody()); 
  }
 }
/*
 public List<ContentVersion> getFile(List<Id> docIdList){
  List<ContentVersion> contentVersionList = [SELECT VersionData, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId = :docIdList AND IsLatest = true];

  return contentVersionList;
 }*/
}