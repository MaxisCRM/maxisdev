global class GetQuantityCount implements cssmgnt.RemoteActionDataProvider {
    @RemoteAction
    global static Map<String,Object> getData(Map<String,Object> inputMap) {
        Map<String, Object> returnMap = new Map<String, Object>();   
        List<csord__Solution__c> solList = new List<csord__Solution__c>();
        String basketId = (String)inputMap.get('basketId');
        Integer BVECount = 0;
        solList = [SELECT id,Name FROM csord__Solution__c WHERE cssdm__product_basket__c =:basketId];
        if(solList != null && !solList.isEmpty()){
            for(csord__Solution__c sol : solList){
                if(sol.Name.contains('Business Voice Connect')){
                    BVECount++;
                }
            }
            returnMap.put('Quantity', BVECount);
        }
        return returnMap;
    }
}