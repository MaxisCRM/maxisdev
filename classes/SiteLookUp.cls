global with sharing class SiteLookUp extends cscfga.ALookupSearch {

    public override String getRequiredAttributes() {
        return '["PDName"]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {
 
         String AccountId = searchFields.get('AccountId') != null ? searchFields.get('AccountId') : '';

        List<cscrm__Site__c> sites = [
            select Id, Name, cscrm__Installation_Address_Details__c
            from cscrm__Site__c 
            where cscrm__Account__c = :AccountId
        ];
        return sites; 
    }
}