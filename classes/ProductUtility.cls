/*
 * EDGE - 79675 : Jayesh : Mapping ETC charges in OLIs
 */
public class ProductUtility{
    public static void CreateOLIs(set<string> setProductBasketId){
        /*
        --select all attributes which are line items
        --check if combinations of Product Family and Line Item Description exist in Product2
        --for the ones which do exist - get Product2.Id
        --for the others - insert and get Product2.Id
        --select standard price book
        --check if there is an existing combination of PriceBook and Product2 in PriceBookEntry
        --if it doesn't exist - create and get PriceBookEntry.Id
        --if it does exist - get PriceBookEntry.Id
        -- enter all PriceBookEntries into OLIs
        */
        
        map<string,string> mapProductBasketIdPriceBookId = new AssignnPriceBookToProductBasketImpl().AssignPriceBook(setProductBasketId);
        system.debug('****mapProductBasketIdPriceBookId=' + mapProductBasketIdPriceBookId);
        system.debug('****setProductBasketId=' + setProductBasketId);
        
        list<cscfga__Attribute__c> lstAttribute = [select Id, cscfga__is_active__c, cscfga__Value__c, cscfga__Is_Line_Item__c, cscfga__Line_Item_Description__c, cscfga__Line_Item_Sequence__c, 
            cscfga__Price__c, cscfga__List_Price__c, cscfga__Product_Configuration__c, cscfga__Product_Configuration__r.cscfga__Product_Basket__c, cscfga__Product_Configuration__r.Name,
            cscfga__Product_Configuration__r.cscfga__Product_Family__c, Name , cscfga__Recurring__c, cscfga__Attribute_Definition__r.cscfga__Line_Item_Sequence__c,
            cscfga__Product_Configuration__r.cscfga__Quantity__c,cscfga__Product_Configuration__r.cscfga__Contract_Term__c,cscfga__Product_Configuration__r.cscfga__Product_Basket__r.csbb__Account__r.Name,cscfga__Product_Configuration__r.cscfga__Product_Basket__r.csbb__Account__c,cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c
            from cscfga__Attribute__c
            where cscfga__Is_Line_Item__c=true and cscfga__is_active__c=true and 
            cscfga__Product_Configuration__r.cscfga__Product_Basket__c in : mapProductBasketIdPriceBookId.keyset()];
            
        system.debug('****lstAttribute=' + lstAttribute);
        
        //this is the map where the keys are: ProductFamily and ProductName (LineItemDescription) 
        map<string,map<string,Product>> mapProductFamilymapProduct = CreateProducts2(lstAttribute);
        system.debug('****mapProductFamilymapProduct=' + mapProductFamilymapProduct);
        
        //this is the map where the keys are: PriceBookId and Product2Id
        map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry = MakePriceBookPBEntriesMap(mapProductBasketIdPriceBookId,lstAttribute,mapProductFamilymapProduct);
        system.debug('****MakePriceBookPBEntriesMap mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);
        
        //this function is void because it just modifies mapPriceBookIdmapPBEntry, so no need for return
        CreatePriceBookEntries(mapPriceBookIdmapPBEntry);
        system.debug('****CreatePriceBookEntries mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);
        
        //this function takes all structures created before and generates OLIs
        CreateOLIs(mapProductBasketIdPriceBookId,lstAttribute,mapProductFamilymapProduct,mapPriceBookIdmapPBEntry);
        
    }
    
    private static void CreateOLIs(map<string,string> mapProductBasketIdPriceBookId, list<cscfga__Attribute__c> lstAttribute,
        map<string,map<string,Product>> mapProductFamilymapProduct, map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry){
        Boolean productConfLevel=false;
        Boolean SumOneOffAndRecurring=false;
        OLI_Sync__c OLISync = OLI_Sync__c.getInstance(UserInfo.getUserId());
        
        if (OLISync != null){
            productConfLevel=OLISync.Product_Configuration_Level__c;    
            SumOneOffAndRecurring=OLISync.Sum_One_Off_And_Recurring__c;
        }
        
        system.debug('****OLISync = ' + OLISync);
        system.debug('****productConfLevel = ' + productConfLevel);
        system.debug('****SumOneOffAndRecurring = ' + SumOneOffAndRecurring);
        
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>(); 
        map<Id,OpportunityLineItem> mapPCIdOLI = new map<Id,OpportunityLineItem>(); 
        OpportunityLineItem tmpOpportunityLineItem;
        set<string> setProductBasketId = new set<string>();
        
        for (cscfga__Attribute__c tmpAttribute : lstAttribute){
            setProductBasketId.add(tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c);
        }
        
        map<Id,cscfga__Product_Basket__c> mapProductBasket = new map<Id,cscfga__Product_Basket__c>([select Id,cscfga__Opportunity__c 
            from cscfga__Product_Basket__c where Id in : setProductBasketId]); 
        
        for (cscfga__Attribute__c tmpAttribute : lstAttribute){
            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
            string tmpLineItemDescription = GetOLILineItemDescription(tmpAttribute);
            string tmpProductBasketId = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c;
            string tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId);
            double tmpUnitPrice = tmpAttribute.cscfga__Price__c;
            map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
            Product tmpProduct = mapProduct.get(tmpLineItemDescription);
            string tmpProduct2Id = tmpProduct.Prod2.Id;
            map<string,PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPriceBookId);
            PBEntry tmpPBEntry = mapPBEntry.get(tmpProduct2Id);
            string tmpPriceBookEntryId = tmpPBEntry.PBE.Id;
            string tmpOpportunityId=mapProductBasket.get(tmpProductBasketId).cscfga__Opportunity__c;

            if(productConfLevel){
                if(mapPCIdOLI.containsKey(tmpAttribute.cscfga__Product_Configuration__c))
                    tmpOpportunityLineItem = mapPCIdOLI.get(tmpAttribute.cscfga__Product_Configuration__c);
                else{
                    tmpOpportunityLineItem = new OpportunityLineItem(); 
                    tmpOpportunityLineItem.UnitPrice=0;
                    tmpOpportunityLineItem.One_Off_Price__c=0;
                    tmpOpportunityLineItem.Recurring_Price__c=0;
                    mapPCIdOLI.put(tmpAttribute.cscfga__Product_Configuration__c,tmpOpportunityLineItem);
                }
            }
            else{
                tmpOpportunityLineItem = new OpportunityLineItem(); 
                tmpOpportunityLineItem.UnitPrice=0;
                tmpOpportunityLineItem.One_Off_Price__c=0;
                tmpOpportunityLineItem.Recurring_Price__c=0;
                lstOLI.add(tmpOpportunityLineItem);
            }
            
            tmpOpportunityLineItem.cscfga__Attribute__c=tmpAttribute.Id;
            tmpOpportunityLineItem.OpportunityId=tmpOpportunityId;
            tmpOpportunityLineItem.PricebookEntryId=tmpPriceBookEntryId;
            tmpOpportunityLineItem.Quantity=tmpAttribute.cscfga__Product_Configuration__r.cscfga__Quantity__c;
            tmpOpportunityLineItem.Description=tmpLineItemDescription;
            tmpOpportunityLineItem.Contract_Tenure_Months__c = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Contract_Term__c;

            if (tmpAttribute.cscfga__Price__c != null){
                
                if(tmpAttribute.cscfga__Recurring__c)
                    tmpOpportunityLineItem.Recurring_Price__c+=tmpAttribute.cscfga__Price__c;
                else
                    tmpOpportunityLineItem.One_Off_Price__c+=tmpAttribute.cscfga__Price__c;
                
                if (SumOneOffAndRecurring)
                    tmpOpportunityLineItem.UnitPrice+=tmpAttribute.cscfga__Price__c;
                else{
                    if (tmpAttribute.cscfga__Recurring__c)
                        tmpOpportunityLineItem.UnitPrice+=tmpAttribute.cscfga__Price__c;
                }
            }  
           //   EDGE-61607      
           // if(tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__r.Product_Type__c=='Modular'){
                //EDGE-79675 Start
              //  if(tmpAttribute.cscfga__Product_Configuration__r.Type__c=='Cancel' && (tmpAttribute.Name.contains('ETC') || tmpAttribute.Name.contains('EarlyTerminationCharge')))
                //{
                    System.debug('ETC line:'+tmpAttribute);
                   // tmpOpportunityLineItem.Early_Termination_Charge__c = decimal.valueof(tmpAttribute.cscfga__Value__c);
                   // tmpOpportunityLineItem.One_Off_Price__c = 0;
                    //tmpOpportunityLineItem.Total_One_Off_Price__c = 0;
                    //tmpOpportunityLineItem.ListPrice = 0;
                    //Field is not writeable:
                   // tmpOpportunityLineItem.Once_Off_Revenue__c = 0;
                    //tmpOpportunityLineItem.Incremental_Revenue__c = 0;
                    //tmpOpportunityLineItem.UnitPrice = 0;
                //}               
                //EDGE-79675 End
               // tmpOpportunityLineItem.Contract_Terms__c=string.valueof(tmpAttribute.cscfga__Product_Configuration__r.cscfga__Contract_Term__c);
                //tmpOpportunityLineItem.Product_Code__c=tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.product_Specification__c;//commented due to EDGE-98254
                //code for Indirect/Direct--start here//
               /* if((tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.csbb__Account__r.RecordType.DeveloperName).contains('Partner')){
                tmpOpportunityLineItem.Channel__c='InDirect';
                tmpOpportunityLineItem.Partner_Info__c=tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.csbb__Account__r.Name;
                tmpOpportunityLineItem.Dealer_Code__c=tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.csbb__Account__r.Mobile_Code__c;
                }
                else{
                tmpOpportunityLineItem.Channel__c='Direct';
                }*/
                //code for Indirect/Direct--End here//
                system.debug('***tmpOpportunityLineItem***'+tmpOpportunityLineItem);
                system.debug('***tmpAttribute***'+tmpAttribute);
               /* if(tmpOpportunityLineItem.Recurring_Price__c!=null && tmpAttribute.cscfga__Product_Configuration__r.Quantity_Product_Configuration__c!=0 && tmpAttribute.cscfga__Product_Configuration__r.Quantity_Product_Configuration__c!=null){
                tmpOpportunityLineItem.Recurring_Price__c=tmpOpportunityLineItem.Recurring_Price__c/tmpAttribute.cscfga__Product_Configuration__r.Quantity_Product_Configuration__c;
                }
                
                if(tmpOpportunityLineItem.One_Off_Price__c!=null && tmpAttribute.cscfga__Product_Configuration__r.Quantity_Product_Configuration__c!=0 && tmpAttribute.cscfga__Product_Configuration__r.Quantity_Product_Configuration__c!=null){ //Addded not null check by Venkata
                    
                tmpOpportunityLineItem.One_Off_Price__c=tmpOpportunityLineItem.One_Off_Price__c/tmpAttribute.cscfga__Product_Configuration__r.Quantity_Product_Configuration__c;
                tmpOpportunityLineItem.New_Income_Revenue__c=tmpOpportunityLineItem.Recurring_Price__c;
                }
                
               if(tmpAttribute.cscfga__Product_Configuration__r.Type__c=='Cancel')
                    tmpOpportunityLineItem.Product_Status__c='Lost';
                else
                    tmpOpportunityLineItem.Product_Status__c='Won';
                    */
            //}   
            //  EDGE-61607              
        }
        
        if (mapPCIdOLI.size() > 0) 
            insert mapPCIdOLI.values();
        else if (lstOLI.size() > 0) 
            insert lstOLI;
    }
    
    
    private static map<string,map<string,PBEntry>> MakePriceBookPBEntriesMap(map<string,string> mapProductBasketIdPriceBookId,list<cscfga__Attribute__c> lstAttribute, 
        map<string,map<string,Product>> mapProductFamilymapProduct){
        list<PBEntry> lstPBEntry = new list<PBEntry>();
        map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry = new map<string,map<string,PBEntry>>(); 
        
        for (cscfga__Attribute__c tmpAttribute : lstAttribute){
            
            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
            string tmpLineItemDescription = GetOLILineItemDescription(tmpAttribute);
            string tmpProductBasketId = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c;
            
            if (mapProductFamilymapProduct.containsKey(tmpProductFamily)){
                map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
                
                if (mapProduct.containsKey(tmpLineItemDescription)){
                    Product tmpProduct = mapProduct.get(tmpLineItemDescription);
                    string tmpProduct2Id = tmpProduct.Prod2.Id;
                    string tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId);
                    
                    PBEntry tmpPBEntry = new PBEntry();
                    tmpPBEntry.PriceBookId=tmpPriceBookId;
                    tmpPBEntry.Product2Id=tmpProduct2Id;
                    
                    if (tmpAttribute.cscfga__Price__c==null)
                        tmpPBEntry.UnitPrice=0;
                    else
                        tmpPBEntry.UnitPrice=tmpAttribute.cscfga__Price__c;
            
                    tmpPBEntry.Name = tmpLineItemDescription;
                    
                    if (mapPriceBookIdmapPBEntry.containsKey(tmpPriceBookId)){
                        map<string,PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPriceBookId);
                        if (!mapPBEntry.containsKey(tmpProduct2Id))
                            mapPBEntry.put(tmpProduct2Id,tmpPBEntry);
                    }
                    else{
                        map<string,PBEntry> mapPBEntry = new map<string,PBEntry>();
                        mapPBEntry.put(tmpProduct2Id,tmpPBEntry);
                        mapPriceBookIdmapPBEntry.put(tmpPriceBookId,mapPBEntry);
                    }
                    
                }
                
            }
        }
        return mapPriceBookIdmapPBEntry;
    }
    
    
    
    private static void CreatePriceBookEntries(map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry){
        set<string> setProduct2Id = new set<string>();
        list<PricebookEntry> lstPricebookEntryInsert = new list<PricebookEntry>(); 
        
        for (map<string,PBEntry> mapPBEntry : mapPriceBookIdmapPBEntry.values()){
            for (PBEntry tmpPBEntry : mapPBEntry.values()){
                setProduct2Id.add(tmpPBEntry.Product2Id);
            }
        }

        if (setProduct2Id.size() > 0){
            map<Id,PricebookEntry> mapPricebookEntry = new map<Id,PricebookEntry> ([select Id, IsActive, Name, Pricebook2Id, Product2Id, UnitPrice 
                from PricebookEntry
                where Product2Id in : setProduct2Id]);

            for (PricebookEntry tmpPricebookEntry : mapPricebookEntry.values()){
                string tmpPricebook2Id = tmpPricebookEntry.Pricebook2Id;
                string tmpProduct2Id = tmpPricebookEntry.Product2Id;

                if (mapPriceBookIdmapPBEntry.containsKey(tmpPricebook2Id)){
                    map<string,PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPricebook2Id);
                    if (mapPBEntry.containsKey(tmpProduct2Id)){
                        PBEntry tmpPBEntry = mapPBEntry.get(tmpProduct2Id);
                        tmpPBEntry.PBEntryId=tmpPricebookEntry.Id;
                    }
                }
            }
            
            for (map<string,PBEntry> mapPBEntry : mapPriceBookIdmapPBEntry.values()){
                for (PBEntry tmpPBEntry : mapPBEntry.values()){
                    if ((tmpPBEntry.PBEntryId=='') || (tmpPBEntry.PBEntryId==null)){
                        PricebookEntry tmpPricebookEntry = new PricebookEntry();
                        tmpPricebookEntry.IsActive = true;
                        tmpPricebookEntry.Pricebook2Id = tmpPBEntry.PriceBookId;
                        tmpPricebookEntry.Product2Id = tmpPBEntry.Product2Id;
                        tmpPricebookEntry.UnitPrice = tmpPBEntry.UnitPrice;
                        tmpPBEntry.PBE = tmpPricebookEntry;
                        lstPricebookEntryInsert.add(tmpPricebookEntry);
                    }   
                    else{
                        PricebookEntry tmpPricebookEntry = mapPricebookEntry.get(tmpPBEntry.PBEntryId);
                        tmpPBEntry.PBE=tmpPricebookEntry;
                    }   
                }
            }
            
        }
        
        if (lstPricebookEntryInsert.size() > 0) 
            insert lstPricebookEntryInsert;
        
    }
    
    private static map<string,map<string,Product>> CreateProducts2(list<cscfga__Attribute__c> lstAttribute){
        map<string,map<string,Product>> mapProductFamilymapProduct = new map<string,map<string,Product>>(); 
        map<String,String> productCodewithFamilyMap= new map<String,String>();
        map<String,String> productTypewithFamilyMap= new map<String,String>();
        //String product_code='';
        for (cscfga__Attribute__c tmpAttribute : lstAttribute){
            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
           // String product_code=tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Definition__r.product_Specification__c;
           // String product_type=tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__r.Product_Type__c;
            string tmpLineItemDescription = GetOLILineItemDescription(tmpAttribute);
        
            if (mapProductFamilymapProduct.containsKey(tmpProductFamily)){
                map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
                if (!mapProduct.containsKey(tmpLineItemDescription)){
                    Product tmpProduct = new Product();
                    tmpProduct.ProductFamily=tmpProductFamily;
                    tmpProduct.LiniItemDescription=tmpLineItemDescription;
                    mapProduct.put(tmpLineItemDescription,tmpProduct);
                }
            }
            else{
                map<string,Product> mapProduct = new map<string,Product>();
                Product tmpProduct = new Product();
                tmpProduct.ProductFamily=tmpProductFamily;
                tmpProduct.LiniItemDescription=tmpLineItemDescription;
                mapProduct.put(tmpLineItemDescription,tmpProduct);
                mapProductFamilymapProduct.put(tmpProductFamily,mapProduct);
               // productCodewithFamilyMap.put(tmpProductFamily,product_code);
               // productTypewithFamilyMap.put(tmpProductFamily,product_type);
            }
            
        }

        if (mapProductFamilymapProduct.size() > 0){
            map<id,Product2> updatemapProduct2=new map<id,Product2>();
            map<Id,Product2> mapProduct2 = new map<Id,Product2>([select Family, Id, IsActive, Name,ProductCode 
                from Product2
                where IsActive=true and Family in : mapProductFamilymapProduct.keySet()]);
            system.debug('mapProduct2'+mapProduct2);
            //EDGE-98254--Start
            for (Product2 tmpProduct2 : mapProduct2.values()){
                if(productTypewithFamilyMap.get(tmpProduct2.Family)=='Modular'){
                    for(String prdcd:productCodewithFamilyMap.keySet()){
                        if(tmpProduct2.Family==prdcd){
                            tmpProduct2.ProductCode=productCodewithFamilyMap.get(tmpProduct2.Family);
                            updatemapProduct2.put(tmpProduct2.id,tmpProduct2);
                        }
                    }
                    
                }
                if (mapProductFamilymapProduct.containsKey(tmpProduct2.Family)){
                    map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProduct2.Family);
                    if (mapProduct.containsKey(tmpProduct2.Name)){
                        Product tmpProduct = mapProduct.get(tmpProduct2.Name);
                        tmpProduct.Product2Id = tmpProduct2.Id;
                    }
                }
            }
            if(updatemapProduct2.size()>0)
                update updatemapProduct2.values();
            //EDGE-98254--End
            list<Product2> lstProduct2Insert = new list<Product2>();
            
            for (string tmpProductFamily : mapProductFamilymapProduct.keySet()){
                map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
                for (Product tmpProduct : mapProduct.values()){
                    if ((tmpProduct.Product2Id=='') || (tmpProduct.Product2Id==null)){
                        Product2 tmpProduct2 = new Product2();
                       
                        tmpProduct2.Family=tmpProductFamily;
                        tmpProduct2.Name = tmpProduct.LiniItemDescription;
                        tmpProduct2.IsActive = true;
                        tmpProduct.Prod2 = tmpProduct2;
                         System.debug('tmpProduct2'+tmpProduct+' '+tmpProduct.LiniItemDescription);
                        lstProduct2Insert.add(tmpProduct2);
                        
                    }
                    else{
                        Product2 tmpProduct2 = mapProduct2.get(tmpProduct.Product2Id);
                        tmpProduct.Prod2 = tmpProduct2;
                    }
                }
            }
            
            if (lstProduct2Insert.size() > 0)                 
                insert lstProduct2Insert;
        }       
        return mapProductFamilymapProduct;
    }
    
    
    public static void DeleteHardOLIs(set<string> setProductBasketId){
        list<cscfga__Product_Basket__c> lstPB = [
            SELECT Id, cscfga__Opportunity__c
            FROM cscfga__Product_Basket__c 
            WHERE Id in : setProductBasketId
        ];
        set<Id> setOpportunityId = new set<Id>();
        for(cscfga__Product_Basket__c tmpPB : lstPB){
            setOpportunityId.add(tmpPB.cscfga__Opportunity__c);
        }
        if (setOpportunityId.size() > 0){
            System.debug(LoggingLevel.DEBUG, 'DeleteHardOLIs.entering oli deletion');
            list<Opportunity> lstOpp = [
                SELECT id, pricebook2id, (SELECT id FROM OpportunityLineItems)
                FROM Opportunity
                WHERE id IN :setOpportunityId
            ];
            if (lstOpp.size()>0){
                list<OpportunityLineItem> lstOLI = new List<OpportunityLineItem>();
                
                System.debug(LoggingLevel.DEBUG, 'DeleteHardOLIs.lstOpp = ' + lstOpp.size());
                for(Opportunity opp: lstOpp){
                    System.debug(LoggingLevel.DEBUG, 'DeleteHardOLIs.inside lstOpp looppps');
                    if(opp.OpportunityLineItems.size() > 0){
                        for(OpportunityLineItem oli : opp.OpportunityLineItems){
                            lstOLI.add(oli);
                        }
                    }
                }
                if(lstOLI.size() > 0)
                    delete lstOLI;
            }
        }
    }
    
    private static string GetOLILineItemDescription(cscfga__Attribute__c tmpAttribute){
        Boolean productConfLevel=false;
        string OLIDescription;
        
        OLI_Sync__c OLISync = OLI_Sync__c.getInstance(UserInfo.getUserId());
        
        if (OLISync != null)
            productConfLevel = OLISync.Product_Configuration_Level__c;  
        
        if (productConfLevel)
            OLIDescription=tmpAttribute.cscfga__Product_Configuration__r.Name;
        else
            OLIDescription=tmpAttribute.cscfga__Line_Item_Description__c;
            
        return OLIDescription;
    }
    
    private class Product{
        public string ProductFamily {get;set;}
        public string LiniItemDescription {get;set;}
        public string Product2Id {get;set;}
        public Product2 Prod2 {get;set;}
    }
    
    private class PBEntry{
        public string PBEntryId {get;set;}
        public string PriceBookId {get;set;}
        public string Product2Id {get;set;}
        public double UnitPrice {get;set;}
        public string Name {get;set;}
        public PriceBookEntry PBE {get;set;}
    }
}