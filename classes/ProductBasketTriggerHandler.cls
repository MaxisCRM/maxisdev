public with sharing class ProductBasketTriggerHandler {
  private static String PRODUCT_BASKET_STAGE = 'Contract Accepted';

  public static void AfterInsertHandle(List<cscfga__Product_Basket__c> lstNewPB) {
    System.debug(LoggingLevel.INFO, 'ProductBasketTriggerHandler.AfterInsertHandle.lstNewPB = ' + lstNewPB);
    UnSyncProductBasketsAfterInsertUpdate(lstNewPB, null);
  }
  
  public static void beforeInsertHandle(List<cscfga__Product_Basket__c> lstNewPB) {
    System.debug(LoggingLevel.INFO, 'ProductBasketTriggerHandler.AfterInsertHandle.lstNewPB = ' + lstNewPB);
    populateCustomerInfo(lstNewPB);
  }

  public static void AfterUpdateHandle(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB) {
    System.debug(LoggingLevel.INFO, 'ProductBasketTriggerHandler.AfterUpdateHandle.lstOldPB = ' + lstOldPB);
    System.debug(LoggingLevel.INFO, 'ProductBasketTriggerHandler.AfterUpdateHandle.lstNewPB = ' + lstNewPB);
    //ModifyBasketNamesForUpdate(lstNewPB,lstOldPB);

    UnSyncProductBasketsAfterInsertUpdate(lstNewPB, lstOldPB);
    DeleteOLIsProductDetailsAfterUpdate(lstNewPB, lstOldPB);
    InsertOLIsProductDetailsAfterUpdate(lstNewPB, lstOldPB);
    revalidateProductBasketsAfterUpdate(lstNewPB, lstOldPB);

  }

  //check if basket is Eligible For Revalidation
  public static void revalidateProductBasketsAfterUpdate(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB) {
    Set<Id> newEligiblePbStageIdSet = new Set<Id>();
    Set<Id> oldEligibleOldPbStageIdSet = new Set<Id>();
    for (cscfga__Product_Basket__c pb : lstOldPB) {
      if ( pb.csordtelcoa__Basket_Stage__c != PRODUCT_BASKET_STAGE ) {
        oldEligibleOldPbStageIdSet.add(pb.Id);
      }
    }

    for (cscfga__Product_Basket__c pb : lstNewPB) {
      if ( pb.csordtelcoa__Basket_Stage__c == PRODUCT_BASKET_STAGE && oldEligibleOldPbStageIdSet.contains(pb.Id) ) {
        newEligiblePbStageIdSet.add(pb.Id);
      }
    }

    if ( !newEligiblePbStageIdSet.isEmpty() ) {
      System.debug(LoggingLevel.INFO, 'ProductBasketTriggerHandler.revalidateProductBasketsAfterUpdate.newEligiblePbStageIdSet = ' + newEligiblePbStageIdSet);
      revalidateProductConfigurations(newEligiblePbStageIdSet);
    }
  }

  public static void revalidateProductConfigurations(Set<Id> productBasketIdSet) {
    System.debug(LoggingLevel.INFO, 'ProductBasketTriggerHandler.revalidateProductConfigurations.productBasketIdSet = ' + productBasketIdSet);
    List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>();

    pcList = [
               SELECT  id
               FROM    cscfga__Product_Configuration__c
               WHERE   cscfga__Product_Basket__c = :productBasketIdSet
                   AND     cscfga__Parent_Configuration__c = ''
                       AND     cscfga__Product_Definition__r.RecordType.Name != 'Package Definition'
             ];

    Set<ID> productConfigurationIdSet = new Set<ID>();
    for (cscfga__Product_Configuration__c pc : pcList) {
      productConfigurationIdSet.add(pc.id);
    }
    Id jobID = cscfga.ProductConfigurationBulkActions.revalidateConfigurationsAsync(productConfigurationIdSet);
    //cscfga.ProductConfigurationBulkActions.revalidateConfigurations(productConfigurationIdSet);
    //System.debug('[ProductBasketTriggerHandler:revalidateProductConfigurations] AsyncJobID:' + jobID);
  }
  /*
  private static void ModifyBasketNames(List<cscfga__Product_Basket__c> lstPB, map<Id,Opportunity> mapOpportunity)
    {
      set<string> setPBId = new set<string>();
      set<string> setOppId = new set<string>();

      for (cscfga__Product_Basket__c tmpPB : lstPB)
      {
        setPBId.add(tmpPB.Id);
      }

      if (setPBId.size()>0)
      {
        map<Id,cscfga__Product_Basket__c> mapProductBasketUpdate = new map<Id,cscfga__Product_Basket__c>([select Id,Name from cscfga__Product_Basket__c
          where Id in : setPBId]);

        list<cscfga__Product_Basket__c> lstProductBasketUpdate = new list<cscfga__Product_Basket__c>();

        for (cscfga__Product_Basket__c tmpPB : lstPB)
        {
          if (tmpPB.cscfga__Opportunity__c!=null)
          {
            if  (mapOpportunity.containsKey(tmpPB.cscfga__Opportunity__c))
            {
              Opportunity tmpOpportunity = mapOpportunity.get(tmpPB.cscfga__Opportunity__c);
              cscfga__Product_Basket__c tmpPBUpdate = mapProductBasketUpdate.get(tmpPB.Id);


              tmpPBUpdate.Name='Basket for ' + tmpOpportunity.Name;

              lstProductBasketUpdate.add(tmpPBUpdate);

              system.debug('***tmpPBUpdate.Name=' + tmpPBUpdate.Name);
            }
          }
        }

        system.debug('***lstProductBasketUpdate=' + lstProductBasketUpdate);
        if (lstProductBasketUpdate.size()>0)
        {
          update lstProductBasketUpdate;

        }
      }
    }
  */

  private static void UnSyncProductBasketsAfterInsertUpdate(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB) {
    //to be called after insert or after update,
    //if newly updated Product Baskets were synced then un-sync all others which have same Opportunity
    //if newly inserted Product Baskets are synced then un-sync all others which have same Opportunity
    set<string> setOpportunityId = new set<string>();
    set<string> setSyncedProductBasketId = new set<string>();
    Boolean Pass;

    for (integer i = 0; i < lstNewPB.size(); ++i) {
      Pass = false;

      if (lstOldPB == null) {
        if  (lstNewPB[i].csordtelcoa__Synchronised_with_Opportunity__c)
          Pass = true;
      } else {
        if ((lstNewPB[i].csordtelcoa__Synchronised_with_Opportunity__c) && (!lstOldPB[i].csordtelcoa__Synchronised_with_Opportunity__c))
          Pass = true;
      }

      if ((Pass) && (lstNewPB[i].cscfga__Opportunity__c != null)) {
        setOpportunityId.add(lstNewPB[i].cscfga__Opportunity__c);
        setSyncedProductBasketId.add(lstNewPB[i].Id);
      }
    }

    if (setOpportunityId.size() > 0) {
      list<cscfga__Product_Basket__c> lstAllProductBasket = [select Id, csordtelcoa__Synchronised_with_Opportunity__c, cscfga__Opportunity__c
          from cscfga__Product_Basket__c where cscfga__Opportunity__c in : setOpportunityId];

      list<cscfga__Product_Basket__c> lstProductBasketUpdate = new list<cscfga__Product_Basket__c>();

      for (cscfga__Product_Basket__c tmpPB : lstAllProductBasket) {
        if (!setSyncedProductBasketId.contains(tmpPB.Id)) {
          if (tmpPB.csordtelcoa__Synchronised_with_Opportunity__c) {
            tmpPB.csordtelcoa__Synchronised_with_Opportunity__c = false;
            lstProductBasketUpdate.add(tmpPB);
          }
        }
      }

      if (lstProductBasketUpdate.size() > 0)
        update lstProductBasketUpdate;
    }
  }
  private static void DeleteOLIsProductDetailsAfterUpdate(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB) {
    set<string> setUnSyncedProductBasketId = new set<string>();

    for (integer i = 0; i < lstNewPB.size(); ++i) {
      if ((!lstNewPB[i].csordtelcoa__Synchronised_with_Opportunity__c) && (lstOldPB[i].csordtelcoa__Synchronised_with_Opportunity__c)) {
        setUnSyncedProductBasketId.add(lstNewPB[i].Id);
      }
    }

    if (setUnSyncedProductBasketId.size() > 0) {
      ProductUtility.DeleteHardOLIs(setUnSyncedProductBasketId);
    }
  }
  private static void InsertOLIsProductDetailsAfterUpdate(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB) {
    System.debug(LoggingLevel.INFO, 'ProductBasketTriggerHandler.InsertOLIsProductDetailsAfterUpdate.lstOldPB = ' + lstOldPB);
    System.debug(LoggingLevel.INFO, 'ProductBasketTriggerHandler.InsertOLIsProductDetailsAfterUpdate.lstNewPB = ' + lstNewPB);
    set<string> setSyncedProductBasketId = new set<string>();
    set<string> setSyncedOppId = new set<string>();

    for (integer i = 0; i < lstNewPB.size(); ++i) {
      if ((lstNewPB[i].csordtelcoa__Synchronised_with_Opportunity__c) && (!lstOldPB[i].csordtelcoa__Synchronised_with_Opportunity__c)) {
        setSyncedProductBasketId.add(lstNewPB[i].Id);
        setSyncedOppId.add(lstNewPB[i].cscfga__Opportunity__c);
      }
    }

    if (setSyncedProductBasketId.size() > 0) {
      //before insert - delete the ones we know for sure will not be needed
      list<cscfga__Product_Basket__c> lstAllProductBasket = [select Id, cscfga__Opportunity__c
          from cscfga__Product_Basket__c where cscfga__Opportunity__c in : setSyncedOppId];

      set<string> setUnsyncedProductBasketId = new set<string>();

      for (cscfga__Product_Basket__c tmpProductBasket : lstAllProductBasket) {
        if (!setSyncedProductBasketId.contains(tmpProductBasket.Id)) {
          setUnsyncedProductBasketId.add(tmpProductBasket.Id);
        }
      }

      if (setUnSyncedProductBasketId.size() > 0)
        ProductUtility.DeleteHardOLIs(setUnSyncedProductBasketId);

      if (setSyncedProductBasketId.size() > 0)
        ProductUtility.CreateOLIs(setSyncedProductBasketId);
    }
  }

  /*
  private static map<Id,Opportunity> GetmapOpportunityForUpdate(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB)
  {
    set<string> setOppId = new set<string>();
    map<Id,Opportunity> mapOpportunity = new map<Id,Opportunity>();

    for (integer i=0;i<lstNewPB.size();++i)
    {
      if ((lstNewPB[i].cscfga__Opportunity__c!=lstOldPB[i].cscfga__Opportunity__c) || (lstNewPB[i].Name=='New Basket'))
      {

      if (lstNewPB[i].cscfga__Opportunity__c!=null)
        setOppId.add(lstNewPB[i].cscfga__Opportunity__c);

      }
    }

    if (setOppId.size()>0)
    {
      mapOpportunity = new map<Id,Opportunity>([select Id,Name from Opportunity
        where Id in : setOppId]);
    }

    return mapOpportunity;

  }
  */

  /*
  private static void ModifyBasketNamesForUpdate(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB)
  {

    map<Id,Opportunity> mapOpportunity = GetmapOpportunityForUpdate(lstNewPB, lstOldPB);
    system.debug('before update mapOpportunity=' + mapOpportunity);
    ModifyBasketNames(lstNewPB,mapOpportunity);

  }
  */
  private static void populateCustomerInfo(List<cscfga__Product_Basket__c> lstNewPB)
  {
 // List<cscfga__Product_Basket__c> updatedPBlst = new List<cscfga__Product_Basket__c>();
 Map<Id,Id> OppAccMap = new Map<Id,Id>();
         Set<Id> oppidset = new Set<Id>();
         for(cscfga__Product_Basket__c basket :lstNewPB){
         oppidset.add(basket.cscfga__Opportunity__c);
         }
     for(Opportunity Opp : [select id,name,AccountId from Opportunity where id in :oppidset]){
     OppAccMap.put(Opp.id,Opp.AccountId);
     }   
    for(cscfga__Product_Basket__c basket :lstNewPB){
    system.debug('basket.cscfga__Opportunity__c'+basket.cscfga__Opportunity__c);
    system.debug('basket.cscfga__Opportunity__r.AccountId'+basket.cscfga__Opportunity__r.AccountId);
    
        basket.csbb__Account__c = OppAccMap.containsKey(basket.cscfga__Opportunity__c)? OppAccMap.get(basket.cscfga__Opportunity__c) : null;
       
    }

  }
}