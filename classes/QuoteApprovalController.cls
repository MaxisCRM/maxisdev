global class QuoteApprovalController extends csbb.CustomButtonExt {
   
    global String performAction (String basketId) {
        //String newUrl = '/apex/CSCAP__SendFormalEmailForApproval?Id=' + basketId ;
        String newUrl = '/apex/CSCAP__SendFormalEmailForApproval_LE?Id=' + basketId ;
        return '{"status":"ok","redirectURL":"' + newUrl + '","text":"Loading Approval Page"}';
    }       
}