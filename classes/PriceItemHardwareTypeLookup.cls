global with sharing class PriceItemHardwareTypeLookup extends cscfga.ALookupSearch {
    public override String getRequiredAttributes() {
        return '[PDName]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {

        system.debug(LoggingLevel.ERROR, 'CommercialProductLookup: ' + JSON.serialize(searchFields));

        String pdName = searchFields.get('PDName');
        system.debug(LoggingLevel.ERROR, 'CommercialProductLookup PDName ' + PDName);
 
        List<cspmb__Price_Item__c> pItems = [
            select Id, Name, Hardware_Type__c
            from cspmb__Price_Item__c 
            where cspmb__Product_Definition_Name__c = :pdName order by Hardware_Type__c asc
        ];

        // get distinct list
        Map<String,cspmb__Price_Item__c> pItemsMap = new  Map<String,cspmb__Price_Item__c>();
        if(pItems!=null)
        {
            for(cspmb__Price_Item__c item : pItems){
                if(!pItemsMap.containskey(item.Hardware_Type__c)){
                pItemsMap.put(item.Hardware_Type__c,item);
                }
            }
        } 
        system.debug('***HardwareMap: ' + pItemsMap);
        if(pItemsMap!=null && pItemsMap.size()>0)       
          return pItemsMap.Values();
        else
          return null;  
    }
}