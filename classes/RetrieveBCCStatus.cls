Global with sharing class RetrieveBCCStatus implements CSPOFA.ExecutionHandler{
    
   
    public List<SObject> process(List<SObject> steps){
            List<CSPOFA__Orchestration_Step__c> result = new List<CSPOFA__Orchestration_Step__c>();
            List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)steps;
            Set<String> brnSet = new Set<String>();
            Set<Id> orchesProcessIdSet= new Set<Id>();
            
            for(CSPOFA__Orchestration_Step__c step: stepList)
            {
                orchesProcessIdSet.add(step.CSPOFA__Orchestration_Process__c);
            }
            system.debug('orchesProcessIdSet ::>'+orchesProcessIdSet);
            for(CSPOFA__Orchestration_Process__c  orch : [Select Id, CSPOFA__Account__r.BRN__c from CSPOFA__Orchestration_Process__c where Id IN:orchesProcessIdSet])
            {
                brnSet.add(orch.CSPOFA__Account__r.BRN__c);
            }       
            
            if(brnSet.isEmpty())
            {
                List<Integration_endpoints__c> endpointList = Integration_endpoints__c.getAll().values();
                for(String brnNumber : brnSet)
                {
                    if (Limits.getCallouts() < Limits.getLimitCallouts() && (brnNumber != null && brnNumber != '') && (!endpointList.isEmpty() && endpointList[0].BCC_Endpoint__c != null && endpointList[0].BCC_Endpoint__c !=  '')) 
                    {
                        //List<Integration_endpoints__c> endpointList = Integration_endpoints__c.getAll().values();
                        HttpRequest req = new HttpRequest();
                        HttpResponse res = new HttpResponse();
                        Http http = new Http();

                        req.setEndpoint(endpointList[0].BCC_Endpoint__c);
                        req.setMethod('GET');                        
                        
                        req.setHeader('Content-Type', 'application/json');
                        req.setHeader('x-source- countrycode', 'MY');
                        req.setHeader('x-source-channel', 'CPQ');
                        req.setHeader('x-source- channelrefid', 'UUID');
                        req.setHeader('x-source- correlationid', 'UUID');
                        req.setHeader('x-source-timestamp', 'yyyyMMddHHmmss');
                        
                        req.setHeader('brnNo', brnNumber);
                        
                        res = http.send(req);
                        if(res.getStatusCode() == 200)
                        {
                            bccResponseWrapper wrapperRec = (bccResponseWrapper)JSON.deserializeStrict(res.getBody(),bccResponseWrapper.class);
                            String brnStatusCode = wrapperRec.brnStatusCode;
                            if(brnStatusCode.equalsIgnoreCase('A'))
                            {
                                //Approved
                            }
                            else 
                            {
                                //Rejected or Others
                            }
                        }
                    }

                }                    
            }
            system.debug('brnSet ::>'+brnSet);                    
            return result;
        }

        public class bccResponseWrapper 
        {
            public String brnNo {get; set;}
            public String brnStatusCode {get; set;}
            public String approveDate {get; set;}
            public String approvedMoreThan5Year {get; set;}
            public String registrationType {get; set;}
            public String typeOfAccount {get; set;}
            public String typeOfCustomer {get; set;}
            public String typeOfCompany {get; set;}
            public String companyName {get; set;}
            public String natureOfBusiness {get; set;}
            public String noOfEmployee {get; set;}
            public String annualSalesTurnover {get; set;}
            public String donor {get; set;}
            public String billAddresss1 {get; set;}
            public String billAddresss2 {get; set;}
            public String billCity {get; set;}
            public String billPostcode {get; set;}
            public String billState {get; set;}
            public String billCountry {get; set;}
            public String deposit {get; set;}
            public String advPayment {get; set;}
            public String puc {get; set;}
            public String finalJustification {get; set;}
            public String preapprovedLines {get; set;}
            public String preapprovedDevices {get; set;}
            public Object authorizeSignatories {get; set;}
            /*public String name {get; set;}
            public String noICorPassport {get; set;}
            public String faxNo {get; set;}
            public String email {get; set;}
            public String fixedLine {get; set;}
            public String mobile {get; set;}*/
    }
}