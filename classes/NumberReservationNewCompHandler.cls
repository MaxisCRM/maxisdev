/*************************************************************************************************
EDGE -93081
Name: NumberReservationBaseComp
Description: Handler for LRM Number reservation on New Tab
Author:Ila/Mahima 
***************************************************************************************************/

public with sharing class NumberReservationNewCompHandler {
    
    //Declare constants    
    public static String REGULAR =  'Regular';
    public static String CONTINUOUS =  'CONTINUOUS';
    public static String PATTERN =  '614';
    public static String POSTPAID =  'Postpaid';
    public static String SEARCH =  'SEARCH';
    public static String AVAILABLE =  'AVAILABLE';
    public static String TELSTRA_NUM =  'Telstra Numbers';
    public static String DIRECT_HOLD =  'DIRECT HOLD';
    
    
    //Wrapper to pass UI input values
    public class SearchMSISDNWrapper {
        @AuraEnabled
        public Integer reqQuantity {get;set;}  
        @AuraEnabled
        public String reqSearch{get;set;}  
        @AuraEnabled
        public String reqPattern{get;set;}
        @AuraEnabled
        public String areaCode{get;set;}
        @AuraEnabled
        public String patternType{get;set;}
        @AuraEnabled
        public Integer pattern{get;set;}
        @AuraEnabled
        public String addressId{get;set;}
        @AuraEnabled
        public String esaValue{get;set;}
    }
    
    //Wrapper to display LRM success response
    public class LRMWrapper{
        @AuraEnabled
        public String numberList{get;set;}
        @AuraEnabled
        public Boolean isSelectedNew{get;set;}
        @AuraEnabled
        public String message{get;set;}
        
    }
 

}