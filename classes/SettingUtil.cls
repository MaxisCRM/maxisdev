public class SettingUtil {
    
    //Custom Setting API Name: Trigger_Setting__c
	//Custom Field: Is_On__c - Checkbox DataType - Default Value: Checked
    public static Boolean isTriggerOn(String triggerName) 
	{
		Boolean result = true; //If no setting, will return default value true;
		
        Trigger_Setting__c setting = Trigger_Setting__c.getInstance(triggerName);
		if(setting != null)
			result = setting.Is_On__c;
		
        return result;
    }

}