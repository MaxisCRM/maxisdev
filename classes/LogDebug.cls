/**
 * Name: LogDebug
 * Author: Accenture
 * Created Date: 11 Dec 2019
 * Description: Utility Class for Logging
 * History:[Author] [Modified Date] [Modification Reason]
**/

public class LogDebug {
    
    static Boolean isDebug = false;
    static Boolean isInfo = true;
    static Boolean isError = true;
    List<Log__c> lstLogs; 
    String className;
    String logCategory;
    
    //Initialise Logging Level based on Custom Setting
    static
    {
        try
        {
            String loggingLevel = DebugLogs__c.getInstance('LOG_LEVEL').value__c;
            //String logginglevel = System.LoggingLevel.toString();
            //System.LoggingLevel level = LoggingLevel.getInstance('LOG_LEVEL');
            //String loggingLevel = Key_Value__c.getInstance('LOG_LEVEL').value__c;
          //  if(logginglevel.Error == true) 
            if(logginglevel == 'DEBUG') 
            {
                isDebug = true; isInfo = true; isError = true; 
            }
            else if(loggingLevel == 'INFO') 
            {
                isDebug = false; isInfo = true; isError = true; 
            }
            else if(loggingLevel == 'ERROR') 
            {
                isDebug = false; isInfo = false; isError = true; 
            } 
        }
        catch(Exception e)
        {
            //Will use default setting instead
        }
    }
    
    //constructor with input : Class Name and Log Category
    public LogDebug(String className, String category)
    {
        this.className = className;
        this.logCategory = category;
        this.lstLogs = new List<Log__c>();
    }
    
    //Log DEBUG Message
    public void debug(String message) 
    {
        if(!isDebug) return;
        this.lstLogs.add(newLog('DEBUG', message, null));
    }
    
    //Log INFO Message
    public void info(String message) 
    {
        if(!isInfo) return;
        this.lstLogs.add(newLog('INFO', message, null));
    }
    
    //Log ERROR Message
    public void error(String message) 
    {
        if(!isError) return;
        this.lstLogs.add(newLog('ERROR', message, null));
    }

    //Log DEBUG Message with related records
    public void debug(String message, String records) 
    {
        if(!isDebug) return;
        this.lstLogs.add(newLog('DEBUG', message, records));
    }
    
    //Log INFO Message with related records
    public void info(String message, String records) 
    {
        if(!isInfo) return;
        this.lstLogs.add(newLog('INFO', message, records));
    }
    
    //Log ERROR Message with related records
    public void error(String message, String records) 
    {
        if(!isError) return;
        this.lstLogs.add(newLog('ERROR', message, records));
    }

    // Logging method for logging exception
    public void log(Exception e, String records)  
    {
        Log__c log = new Log__c();
        
        log.Class__c = this.className;
        log.Category__c = this.logCategory;
        log.Exception_Type__c = e.getTypeName();
        log.Stack_Trace__c = e.getStackTraceString();
        log.Message__c = e.getMessage();
        log.Level__c = 'ERROR';
        log.Records__c = records;
        
        this.lstLogs.add(log);
    }
    
    //Method used to save the logs. This shall be called only once per transaction at the end
    public void save() 
    {
        if(!this.lstLogs.isEmpty())
        {
            try{
                insert this.lstLogs;
            }
            catch (Exception e) {}
        }
    }
    
    //Creating new Log record
    private Log__c newLog(String loggingLevel, String message, String records)
    {
        Log__c log = new Log__c();
        log.Class__c = this.className;
        log.Category__c = this.logCategory;
        log.Message__c = message;
        log.Level__c = loggingLevel;
        log.Records__c = records;
        return log;
    }
}