//@RestResource(/*urlMapping=''*/)

public class SmsCalloutClass 
{
    public class requestClass 
    {
        public String SMS_Id;
        public String applnType;
        public String profile;
        public String subProfile;
        public String msisdn;
        public String languageType;
        public String smsContent;
        public String messageId;
        public String nameId;    
    }
    
    //private final Sms_Log__c thisCase;
    public Boolean toSend = false;
    public SMS_Log__c cas {get; set;}
    public Id owner {get; set;}
    public Id Id { get; set; }
    private final Sms_Log__c LatestLog;
    
    public SmsCalloutClass(ApexPages.StandardController controller) 
    {
        cas =  (SMS_Log__c) controller.getRecord();
        System.debug('start class');
       /* Id = cas.Id;
        System.debug('The case record: ' + cas.SMS_Content__c);
        toSend = true;
        System.debug('The case record: ' + toSend);
        owner = cas.Id;*/
    }
    
    public static HttpResponse SendSmsToExternalSystem(Sms_Log__c[] LogList)
    {
        List <Sms_Log__c> sList = [SELECT ID,Name,Message_ID__c,Application_Type__c,Profile__c,
                                   Sub_Profile__c,Mobile_Number__c,Language_Type__c,SMS_Content__c
                                   FROM Sms_Log__c
                                   WHERE ID =: LogList];
        LogDebug log = new LogDebug(LoggerSampleUsage.class.getName(), 'Exception');
        log.debug('===> Start doExceptionLogging...');   
        String relatedRecords = sList[0].Id;
        
        try
        {
            Http h = new Http();
            HttpRequest request = new HttpRequest();
            request.setTimeout(120000);
                
            System.debug('Create Request');
            requestClass requestParam = new requestClass();
            requestParam.SMS_Id = sList[0].Id;
            requestParam.applnType = sList[0].Application_Type__c;
            requestParam.profile = sList[0].Profile__c;
            requestParam.subProfile = sList[0].Sub_Profile__c;
            requestParam.msisdn = sList[0].Mobile_Number__c;
            requestParam.languageType = sList[0].Language_Type__c;
            requestParam.smsContent = sList[0].SMS_Content__c;
            requestParam.messageId = sList[0].Message_ID__c;
            requestParam.nameId = sList[0].Name;
            String outbound = (String)JSON.serialize(requestParam);
            System.debug('Body:'+outbound);   
                
            request.setEndpoint('https://putsreq.com/IGFo2q1A9AFNv8Fnju7Y');
            request.setMethod('POST');
            request.setBody(outbound);
            System.debug('before httpresponse');
    
            HttpResponse res = h.send(request);
            System.debug('after httpresponse');
            if (res.getStatusCode() != 200)
                {
                    system.debug(res.getBody());
            	}
            else
            	{
                	system.debug(res.getBody());
                }
            return res;
        }catch(Exception e)
            {
                log.log(e, relatedRecords);
                log.debug('===> End doExceptionLogging...');
            }
        log.save();
        return null;        
    }
	
    public PageReference updateStatus()
    {
        String sta = 'Sent';
        List<Sms_Log__c> LatestLog = [Select Id, Name, Status__c, SMS_Content__c, Contact__c From Sms_Log__c Where Id =:ApexPages.currentPage().getParameters().get('Id')];
        System.Debug(LatestLog);
        System.Debug('Status confirmed as sent');
        
        LogDebug log = new LogDebug(LoggerSampleUsage.class.getName(), 'Exception');
        log.debug('===> Start doExceptionLogging...');   
        String relatedRecords = LatestLog[0].Id;
        
        try{
            for(Sms_Log__c l : LatestLog)
            {
                if (l.Id != null)
                {
                    l.Status__c = sta;
                    update LatestLog;
                }
            }   
    	}catch(DmlException ex)
        {
            System.debug('SMS to go through validation rule');
            log.log(ex, relatedRecords);
            ApexPages.addMessages(ex);
            System.debug(ex);
        }
        log.save();
        return new ApexPages.StandardController(cas).view();
    }
    
    public PageReference cancel() 
    {
        PageReference acctPage = new ApexPages.StandardController(cas).view();
        acctPage.setRedirect(true);
        return acctPage;
    }
}