/*
* File Name   : TestDataFactory.apxc
* Author	  : ACN Janice Fong
* Created Date: 06/01/2020
* Description : Test class for Mother/Father/God of all Test Data
*/

@isTest

public with sharing class TestDataFactory {
    public static User createSystemAdminUser(){
        Profile systemAdminId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        String uniqueUserName1 = 'standard' + DateTime.now().getTime() + '@testorg.com';
        User usr1 = new User(LastName = 'LIVESTON',
                                    FirstName='JASON',
                                    Alias = 'jliv',
                                    Email = 'jason@testing.com',
                                    Username = uniqueUserName1+2,
                                    ProfileId = systemAdminId.id,
                                    TimeZoneSidKey = 'GMT',
                                    LanguageLocaleKey = 'en_US',
                                    EmailEncodingKey = 'UTF-8',
                                    LocaleSidKey = 'en_US'
                                     ); 
        insert usr1;
        return usr1;
    }
    
    public static User createCommercialTeamUser(){
        Profile commercialTeamId = [SELECT Id FROM Profile WHERE Name = 'Commercial Team' LIMIT 1];
        String uniqueUserName1 = 'standard' + DateTime.now().getTime() + '@testorg.com';
        User usr1 = new User(LastName = 'User',
                                    FirstName='Test',
                                    Alias = 'testusr',
                                    Email = 'test.user@maxis.com',
                                    Username = uniqueUserName1+2,
                                    ProfileId = commercialTeamId.id,
                                    TimeZoneSidKey = 'GMT',
                                    LanguageLocaleKey = 'en_US',
                                    EmailEncodingKey = 'UTF-8',
                                    LocaleSidKey = 'en_US'
                                     ); 
        insert usr1;
        return usr1;
    }
    
    public static List<Lead> createSMELead(Integer numOfLead){
        List<Lead> listLead = new List<Lead>();
        for(Integer i=0;i<numOfLead;i++){
			Lead lead = new Lead(Salutation = 'Mr.',
                                 FirstName = 'Test1'+i,
                                 LastName = 'Customer1'+i,
                                 Email ='test1'+i+'.customer1'+i+'@maxis.com',
                                 MobilePhone = '012345678'+i,
                                 phone = '032304230'+i,
                                 status = 'New',
                                 BRN__c = '1234123'+i,
                                 BRN_Type__c = 'SSM',
                                 Product__c = 'Fixed',
                                 Company = 'Maxis'+i,
                                 Product_Of_Interest__c = 'CloudPOS',
                                 Company_Revenue_per_Annum__c = 'Between RM0.5 mil to RM50 mil',
                                 No_of_Employees__c = 'Between 5 to 100 employees',
                                 Industry = 'IT & Communications'
                                );   
           listLead.add(lead);
        }
        return listLead;
    }
    
    public static List<Lead> createNonSMELead(Integer numOfLead){
        List<Lead> listLead = new List<Lead>();
        for(Integer i=0;i<numOfLead;i++){
			Lead lead = new Lead(Salutation = 'Mr.',
                                 FirstName = 'Test2'+i,
                                 LastName = 'Customer2'+i,
                                 Email ='test2'+i+'.customer2'+i+'@maxis.com',
                                 MobilePhone = '012345678'+i,
                                 phone = '032304230'+i,
                                 status = 'New',
                                 BRN__c = 'H1234123'+i,
                                 BRN_Type__c = 'Government',
                                 Product__c = 'Fixed',
                                 Company = 'Maxis2'+i,
                                 Product_Of_Interest__c = 'CloudPOS',
                                 Company_Revenue_per_Annum__c = 'Between RM0.5 mil to RM50 mil',
                                 No_of_Employees__c = 'Between 5 to 100 employees',
                                 Industry = 'IT & Communications'
                                );   
           listLead.add(lead);
        }
        return listLead;
    }
    
    public static List<Account> createSMEAccount(Integer numOfAcc){
        List<Account> listAcc = new List<Account>();
        for(Integer i=0;i<numOfAcc;i++){
			Account acc = new Account(Name = 'Maxis '+i,
                                      Type ='Active',
                                      website = 'www.maxis.com',
                                      phone = '032304231'+i,
                                      BRN__c = '1234124'+i,
                                      BRN_Type__c = 'SSM',
                                      Segment__c = 'SME',
                                      Company_Revenue_per_Annum__c = 'Between RM0.5 mil to RM50 mil',
                                      No_of_Employees__c = '> 100 employees',
                                      Industry = 'IT & Communications'
                                     );   
           listAcc.add(acc);
        }
        
        insert listAcc;
        return listAcc;
    }
    
    public static List<Account> createNonSMEAccount(Integer numOfAcc){
        List<Account> listAcc = new List<Account>();
        for(Integer i=0;i<numOfAcc;i++){
			Account acc = new Account(Name = 'Maxis2 '+i,
                                      Type ='Active',
                                      website = 'www.maxis.com',
                                      phone = '032304231'+i,
                                      BRN__c = 'H1234124'+i,
                                      BRN_Type__c = 'Government',
                                      Segment__c = 'Government',
                                      Company_Revenue_per_Annum__c = 'Between RM0.5 mil to RM50 mil',
                                      No_of_Employees__c = '> 100 employees',
                                      Industry = 'IT & Communications'
                                     );   
           listAcc.add(acc);
        }
        
        insert listAcc;
        return listAcc;
    }
    
    public static List<Contact> createContact(Integer numOfContact, Account acc){
        List<Contact> listContact = new List<Contact>();
        for(Integer i=0;i<numOfContact;i++){
			Contact contact = new Contact(Salutation = 'Mr.',
                                          FirstName = 'Test'+i,
                                          LastName = 'Customer'+i,
                                          Email ='test'+i+'.customer'+i+'@maxis.com',
                                          MobilePhone = '012345678'+i,
                                          phone = '032304230'+i,
                                          Product_Of_Interest__c = 'CloudPOS',
                                          Account = acc
                                        );   
           listContact.add(contact);
        }
        
        insert listContact;
        return listContact;
    }
}