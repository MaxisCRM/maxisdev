global with sharing class PriceItemContractTermLookup extends cscfga.ALookupSearch {
    public override String getRequiredAttributes() {
        return '["PDName"]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {

        system.debug(LoggingLevel.ERROR, 'CommercialProductLookup: ' + JSON.serialize(searchFields));

        String pdName = searchFields.get('PDName');
        system.debug(LoggingLevel.ERROR, 'CommercialProductLookup PDName ' + PDName);
 
        List<cspmb__Price_Item__c> pItems = [
            select Id, Name, Service_Type__c, Bandwidth__c, cspmb__Contract_Term__c
            from cspmb__Price_Item__c 
            where cspmb__Product_Definition_Name__c = :pdName and cspmb__Contract_Term__c != null and Service_Type__c = null order by cspmb__Contract_Term__c asc
        ];

        // get distinct list
        Map<String,cspmb__Price_Item__c> pItemsMap = new  Map<String,cspmb__Price_Item__c>();
        if(pItems!=null)
        {
            for(cspmb__Price_Item__c item : pItems){
                if(!pItemsMap.containskey(item.cspmb__Contract_Term__c)){
                pItemsMap.put(item.cspmb__Contract_Term__c,item);
                }
            }
        } 
        if(pItemsMap!=null && pItemsMap.size()>0)       
          return pItemsMap.Values();
        else
          return null;  
    }
}