/*
* File Name   : LeadBulkImportTriggerControllerTest.apxc
* Author	  : ACN Janice Fong
* Created Date: 06/01/2020
* Description : Test class for LeadBulkImportTriggerController
*/

@isTest(SeeAllData=false)
private without sharing class LeadBulkImportTriggerControllerTest {
	@isTest 
    private static void testCheckOwnerAsSystemAdmin() {
	   	System.runAs(TestDataFactory.createSystemAdminUser()){
            List<Lead> newLead = new List<Lead>();
            User commerceTeam = TestDataFactory.createCommercialTeamUser();
			System.debug('commerceTeam '+ commerceTeam.Id);
            //SME Account/Lead
            Group queue = [SELECT Id,DeveloperName,Name,Type FROM Group where type = 'Queue' AND DeveloperName = 'SME_Queue' LIMIT 1]; 
	        List<Lead> listSMELead = TestDataFactory.createSMELead(2);
            List<Account> listSMEAcc = TestDataFactory.createSMEAccount(3);
            listSMELead[1].ownerId = commerceTeam.Id;
            newLead.addAll(listSMELead);
            
            //Non-SME Account/Lead
            List<Lead> listNonSMELead = TestDataFactory.createNonSMELead(3);
            List<Account> listNonSMEAcc = TestDataFactory.createNonSMEAccount(3);
            listNonSMELead[0].Company = listNonSMEAcc[0].name;
            listNonSMELead[1].OwnerId = commerceTeam.Id;
            newLead.addAll(listNonSMELead);
            
            Test.startTest();
            //SME Account/Lead
            insert newLead;
            Test.stopTest();

            List<Lead> listLead = [SELECT Id, Email,OwnerId FROM Lead WHERE Id IN: newLead];
            List<Account> tempListNonSMEAcc = [SELECT Id, OwnerId FROM Account WHERE Id =: listNonSMEAcc[0].Id LIMIT 1];
            for(Lead lead: listLead){
                if(lead.email == listSMELead[0].email){
                    //SME Account/Lead - for no specific owner and no match in existing accounts
                    System.assertEquals(queue.id, lead.OwnerId); 
                }else if(lead.email == listSMELead[1].email){
                    //SME Account/Lead - for specific owner
                    System.assertEquals(commerceTeam.id, lead.OwnerId);
                }else if(lead.email == listNonSMELead[0].email){
                    //Non-SME Account/Lead - for match in existing account
                    System.assertEquals(tempListNonSMEAcc[0].OwnerId, lead.OwnerId);
                }else if(lead.email == listNonSMELead[1].email){
                    //Non-SME Account/Lead - for specific owner
                    System.assertEquals(commerceTeam.id, lead.OwnerId);
                }else if(lead.email == listNonSMELead[2].email){
                    //Non-SME Account/Lead - for no specific owner and no match in existing accounts
                    System.assertEquals(queue.id, lead.OwnerId);
                }
            }
    	}
    }
    
    @isTest 
    private static void testCheckOwnerAsCommercialTeam() {
        
        System.runAs(TestDataFactory.createCommercialTeamUser()){
			
	        List<Lead> listLead = TestDataFactory.createSMELead(2);
            List<Account> listAcc = TestDataFactory.createSMEAccount(3);
            Test.startTest();
            insert listLead[0];
            listLead[1].Company = listAcc[0].name;
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.AllowSave = true; 
            Database.SaveResult sr = Database.insert(listLead[1], dml); 
            if (sr.isSuccess()) {   
    			listLead = [SELECT Id, OwnerId FROM Lead LIMIT 2];
                System.assertEquals(UserInfo.getUserId(), listLead[0].OwnerId);
                System.assertEquals(UserInfo.getUserId(), listLead[1].OwnerId);
            }
			Test.stopTest();
    	}
    }
}