<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>To_send_email_alert_saying_lead_conversion_done</fullName>
        <description>To send email alert saying lead conversion done</description>
        <protected>false</protected>
        <recipients>
            <recipient>aisyah.abu.bakar@maxis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nur.maisarah.rashid@maxis.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SUPPORTSelfServiceNewUserLoginSAMPLE</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Name_to_Upper</fullName>
        <field>Name</field>
        <formula>UPPER (Name)</formula>
        <name>Account Name to Upper</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_Account_Name_to_Upper</fullName>
        <description>Change previous account Name to Upper</description>
        <field>Previous_Company_Name__c</field>
        <formula>UPPER (Previous_Company_Name__c)</formula>
        <name>Previous Account Name to Upper</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_phone_number_format</fullName>
        <description>[13-Dec-19 KSK] To remove Salesforce OOTB phone field formatting.</description>
        <field>Phone</field>
        <formula>SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Phone, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;)</formula>
        <name>Remove phone number format</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Type_to_Prospect</fullName>
        <field>Type</field>
        <literalValue>Prospect</literalValue>
        <name>Type to Prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_field_to_be_active</fullName>
        <description>[10-Dec-19 KSK] To make an account active</description>
        <field>Type</field>
        <literalValue>Active</literalValue>
        <name>Update field to be active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_field_to_be_reviewed</fullName>
        <description>[10-Dec-19 KSK] For Portfolio Manager to review</description>
        <field>Type</field>
        <literalValue>Pending Review</literalValue>
        <name>Update field to be reviewed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_type_to_Prospect</fullName>
        <field>Type</field>
        <literalValue>Prospect</literalValue>
        <name>Update type to Prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Convert Account Name to Upper</fullName>
        <actions>
            <name>Account_Name_to_Upper</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_Account_Name_to_Upper</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remove phone format</fullName>
        <actions>
            <name>Remove_phone_number_format</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>[13-Dec-19 KSK] Auto removes brackets and dashes from Salesforce phone field.</description>
        <formula>IF(NOT(isBlank( Phone )), true,false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
