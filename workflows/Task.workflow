<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <tasks>
        <fullName>To_proceed_with_adding_contacts_to_this_account</fullName>
        <assignedTo>aisyah.abu.bakar@maxis.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>To proceed with adding contacts to this account</subject>
    </tasks>
</Workflow>
