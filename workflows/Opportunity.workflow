<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Record_Type_to_Edit</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Edit_Opportunity_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type to Edit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage_Won</fullName>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Opportunity Stage Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Stages_from_Q_to_PP</fullName>
        <description>Update stages from Qualification to Proposal Preparation</description>
        <field>StageName</field>
        <literalValue>Proposal Preparation</literalValue>
        <name>update Stages from Q to PP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Oppty Record type from New to Edit</fullName>
        <actions>
            <name>Change_Record_Type_to_Edit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>[16-Dec-19 KSK] Change oppty record type from new to edit</description>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Forecast to Omitted for forecast</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>&quot;MYR 10,000,000&quot;</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
